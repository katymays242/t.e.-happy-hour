-- *****************************************************************************
-- This script contains INSERT statements for populating tables with seed data
-- *****************************************************************************

BEGIN;

-- INSERT statements go here

INSERT INTO account (account_id, account_type) VALUES (1, 'Customer');
INSERT INTO account (account_id, account_type) VALUES (2, 'Owner');
INSERT INTO account (account_id, account_type) VALUES (3, 'Admin');

INSERT INTO customer (customer_id, first_name, last_name, email, password, salt, account_id) VALUES (1, 'Jason', 'Mann', 'jason@email.com', 'NYOQiFnWnmfe1ZMT+wKHYw==', '0DMXjf9rMijMCg51z8gTSEArowCeXpOfnixGZdtRHvRpYye98vSOUtU4akU4WT2G1K1Ac7qmtcQQLb9PezJ1v4HAajRZaLMX+jdlnjpTCKRUPvXtuP1nR6IIkgvAEjitaFDEReFDIvnvT17q0cJM8oKX7SoRXwcO+p7QlnXbjsU=', 1);
INSERT INTO customer (customer_id, first_name, last_name, email, password, salt, account_id) VALUES (2, 'Matt', 'Mayer', 'matt@email.com', 'KfHH8CqTkfd5Qhx/5ToMWA==', 'xSqUmAVtrPKDvUsBUfwXB+YFt7v2K6vom8KvNUuG5HjN2hMWIpToHZJ2v9AM8lufMBZIMZqR4PlSL/EfXSMRP8YRLAUdKYfEptEj7ENck88FDVQqso+gCVVz1LIDr1Wz/szXedL/d0skDFhXSBbe1IOmRi+A5A1OMq6koJch054=', 1);
INSERT INTO customer (customer_id, first_name, last_name, email, password, salt, account_id) VALUES (3, 'Katy', 'Mays', 'katy@email.com', '60tUibnNBDk0X6l8SMHN9g==', 'xrsD/3p5+V+udZTrS3UCPg6kymUbUEk5UgGMYMigG2lWtHM2FEfoEPguwUiMimnImjE27COUx3za7+O4eXX24ug5FTUKjsEq1f4OQlx3KvmSP+PIWTXgaxLqnwSlhMtPxdKDocK+rk1FGtY6EPI8oNVdh/YkbDuyP1eDGrsp/lU=', 1);
INSERT INTO customer (customer_id, first_name, last_name, email, password, salt, account_id) VALUES (4, 'Rob', 'Sattely', 'rob@email.com', 'B6fto3GSBiN1wAewF3H1QA==', 'xSqUmAVtrPKDvUsBUfwXB+YFt7v2K6vom8KvNUuG5HjN2hMWIpToHZJ2v9AM8lufMBZIMZqR4PlSL/EfXSMRP8YRLAUdKYfEptEj7ENck88FDVQqso+gCVVz1LIDr1Wz/szXedL/d0skDFhXSBbe1IOmRi+A5A1OMq6koJch054=', 1);
SELECT setval(pg_get_serial_sequence('customer', 'customer_id'), 4);

INSERT INTO neighborhood (neighborhood_id, name) VALUES (1, 'Campus');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (2, 'Bexley');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (3, 'Gahanna');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (4, 'Downtown');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (5, 'Easton');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (6, 'Short North');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (7, 'Grandview');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (8, 'Clintonville');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (9, 'Upper Arlington');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (10, 'Italian Village');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (11, 'German Village');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (12, 'Victorian Village');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (13, 'Brewery District');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (14, 'Arena District');
INSERT INTO neighborhood (neighborhood_id, name) VALUES (15, 'Dublin');

INSERT into restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials, websitelink)
VALUES (DEFAULT,'Condado Tacos', '1227 N. High Street', 'Columbus', 'OH', '43201', '614-928-3909', 'Condado Tacos.jpg', 'A red hot local restaurant serving tacos and drinks', '1/2 off all margaritas, $1 off tacos', '$1 off coupon for Columbus Happy Hour users. Special code: #TECHH', 'http://www.condadotacos.com/');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 6);

INSERT INTO restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials)
VALUES (DEFAULT, 'Arch City Tavern', '862 N High St', 'Columbus', 'OH', '43215', '614-725-5620', 'Arch City Tavern.jpg', 'Experience the history of Columbus in a hand crafted setting', 'Half off all drafts, bottled wine and $6 appetizers', '$1 off coupon for Columbus Happy Hour users. Special code: #TECHH');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 6);


INSERT INTO restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials)
VALUES (DEFAULT, 'Barrel on High', '1120 N High St', 'Columbus', 'OH', '43201', '614-564-9058', 'Barrel on High.jpg', 'Whiskey, bourbon & other spirits (plus small plates, burgers & American grub) in a former speakeasy', '$5 apps and $4 all draft beers', '$1 off coupon for Columbus Happy Hour users. Special code: #TECHH');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 6);


INSERT INTO restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials)
VALUES (DEFAULT, 'Bodega', '1044 N High St', 'Columbus', 'OH', '43201', '614-299-9399', 'Bodega.jpg', 'Hip gastropub serving more than 150 beers & an inventive American menu in a lively atmosphere', 'Half-off wings and $5 house margaritas', '$1 off coupon for Columbus Happy Hour users. Special code: #TECHH');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 6);


INSERT INTO restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials)
VALUES (DEFAULT, 'Brewcadia','467 N High St', 'Columbus', 'OH', '43215', '614-228-8831', 'Brewcadia.jpg', 'Short North premier arcade bar where old school amusement meets downtown culture', 'Half-price pizza', '$1 off coupon for Columbus Happy Hour users. Special code: #TECHH');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 6);


INSERT INTO restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials)
VALUES (DEFAULT, 'Hubbard Grille', '793 N High St', 'Columbus', 'OH', '43215', '614-291-5000', 'Hubbard Grille.jpg', 'A restaurant serving modern American cuisine in Short North', '$2 PBR Cans, $3 Drafts, $4 House Wine, $5 Menu Cocktails, and app specials', '$1 off coupon for Columbus Happy Hour users. Special code: #TECHH');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 6);


INSERT INTO restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials)
VALUES (DEFAULT, 'Martini Modern Italian', '445 N High St', 'Columbus', 'OH', '43215', '614-224-8259', 'Martini Modern Italian.jpg', 'Traditional Italian dishes with a modern twist served in a contemporary space with chandeliers', '$5 martinis, select handcrafted cocktails and half-price appetizers in our bar', '$1 off coupon for Columbus Happy Hour users. Special code: #TECHH');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 6);

INSERT INTO restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials)
VALUES (DEFAULT, 'Oddfellows Liquor Bar', '1038 N High St', 'Columbus', 'OH', '43201', '614-923-9631', 'Oddfellows Liquor Bar.jpg', 'Brick-lined joint with old-timey kitsch & a patio for craft draft beer, clever cocktails & trivia', '$3 cocktails 1/2 off draft beers $5 glasses of wine', '$1 off coupon for Columbus Happy Hour users. Special code: #TECHH');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 6);

INSERT INTO restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials)
VALUES (DEFAULT, 'Rodizio Grill', '125 W Nationwide Blvd', 'Columbus', 'OH', '43215', '614-241-4400', 'Rodizio Grill.jpg', 'Outpost of a Brazilian steakhouse chain offering skewered meats carved tableside plus a salad bar', '$3 cocktails 1/2 off draft beers $5 glasses of wine', '$1 off coupon for Columbus Happy Hour users. Special code: #TECHH');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 14);

INSERT INTO restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials)
VALUES (DEFAULT, 'AAB India Restaurant', '1470 Grandview Ave', 'Columbus', 'OH', '43212', '614-486-2800', 'AAB India Restaurant.jpg', 'Classic Northern Indian fare like curries, biryani & tandoor-fired bites, as well as a lunch buffet', '1/2 off draft beers $5 glasses of wine', '$1 off coupon for Columbus Happy Hour users. Special code: #TECHH');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 7);

INSERT INTO restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials)
VALUES (DEFAULT, 'The Three Legged Mare', '401 N. FRONT STREET ', 'Columbus', 'OH', '43215', '614-222-4950', '3 Legged Mare.jpg', 'If you love Irish pubs with tasty fare and flowing beer, stop by The Three-Legged Mare', 'Select Half-price Appetizers - $1.00 off all Draughts - $1.00 off House Wine - $1.00 off Well Liquor - $5.00 Specialty Martinis', '$1 off coupon for Columbus Happy Hour users. Special code: #3LEGS');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 14);

INSERT INTO restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials)
VALUES (DEFAULT, 'R Bar', '413 N. FRONT ST.', 'Columbus', 'OH', '43215', '614-222-4950', 'r bar.jpg', 'R Bar has taken shape as the local neighborhood bar with a great hockey vibe that can accommodate a Blue Jackets crowd or regular neighborhood patrons.', '$2 Domestic Bottle - $3 Domestic Draughts - $4 Import Bottles - $4 Premium Draughts - 1/2 Off Liquor and Wine', '$4 off coupon for Columbus Happy Hour users. Special code: #RBAR');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 14);

INSERT INTO restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials)
VALUES (DEFAULT, 'Figlio', '1369 Grandview Ave', 'Columbus', 'OH', '43212', '614-481-8850', 'Figlio.jpg', 'Updated brick-oven pies & modern Italian cuisine in a space that is equally date- & family-friendly', '$1 off certain glasses of wine', '$1 off coupon for Columbus Happy Hour users. Special code: #TECHH');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 7);

INSERT INTO restaurant (restaurant_id, restaurant_name, addressLine1, city, state, postalCode, restaurant_phone_number, restaurantimage, restaurant_text, happyhour_text, specials)
VALUES (DEFAULT, 'Matt the Miller Tavern', '1400 Grandview Ave', 'Columbus', 'OH', '43212', '614-754-1026', 'Matt the Miller Tavern.jpg', 'Upscale tavern serving an extensive menu, ranging from flatbreads to steaks, plus numerous beers.', '1/2 off draft beers and desserts', '$1 off coupon for Columbus Happy Hour users. Special code: #TECHH');
INSERT into restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (currval('restaurant_restaurant_id_seq'), 7);


INSERT INTO price (price_id, price_range) VALUES (1, '$1 - $20');
INSERT INTO price (price_id, price_range) VALUES (2, '$21 - $50');
INSERT INTO price (price_id, price_range) VALUES (3, '$50 - $100');
INSERT INTO price (price_id, price_range) VALUES (4, '$101 and up');

INSERT INTO food_type (food_type_id, food_type_name) VALUES (1, 'American');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (2, 'African');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (3, 'Asian');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (4, 'BBQ');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (5, 'Bar Food');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (6, 'Breakfast');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (7, 'Burger');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (8, 'Cafe');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (9, 'Caribbean');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (10, 'Chinese');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (11, 'Coffee and Tea');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (12, 'Diner');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (13, 'Drinks Only');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (14, 'European');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (15, 'Greek');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (16, 'Indian');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (17, 'Italian');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (18, 'Japanese');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (19, 'Latin American');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (20, 'Mexican');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (21, 'Middle Eastern');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (22, 'Pizza');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (23, 'Sandwich');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (24, 'Seafood');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (25, 'Soul Food');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (26, 'Steak');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (27, 'Sushi');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (28, 'Tacos');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (29, 'Thai');
INSERT INTO food_type (food_type_id, food_type_name) VALUES (30, 'Vegetarian');

INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (1, 28);
INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (2, 1);
INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (3, 5);
INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (4, 1);
INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (5, 22);
INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (6, 1);
INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (7, 17);
INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (8, 13);
INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (9, 19);
INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (10, 16);
INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (11, 5);
INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (12, 5);
INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (13, 17);
INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (14, 5);


INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (1, 3);
INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (2, 4);
INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (3, 2);
INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (4, 3);
INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (5, 2);
INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (6, 1);
INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (7, 2);
INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (8, 1);
INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (9, 2);
INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (10, 4);
INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (11, 2);
INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (12, 3);
INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (13, 4);
INSERT INTO restaurant_price (restaurant_id, price_id) VALUES (14, 2);


INSERT INTO day_of_week (day_of_week_id, day_of_week) VALUES (0, 'Monday');
INSERT INTO day_of_week (day_of_week_id, day_of_week) VALUES (1, 'Tuesday');
INSERT INTO day_of_week (day_of_week_id, day_of_week) VALUES (2, 'Wednesday');
INSERT INTO day_of_week (day_of_week_id, day_of_week) VALUES (3, 'Thursday');
INSERT INTO day_of_week (day_of_week_id, day_of_week) VALUES (4, 'Friday');
INSERT INTO day_of_week (day_of_week_id, day_of_week) VALUES (5, 'Saturday');
INSERT INTO day_of_week (day_of_week_id, day_of_week) VALUES (6, 'Sunday');

INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (1, 0, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (1, 1, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (1, 2, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (1, 3, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (1, 4, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (1, 5, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (1, 6, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (2, 0, '10:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (2, 1, '9:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (2, 2, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (2, 3, '9:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (2, 4, '8:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (2, 5, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (2, 6, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (3, 0, '11:00AM', '11:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (3, 1, '11:00AM', '9:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (3, 2, '11:00AM', '4:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (3, 3, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (3, 4, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (3, 5, '11:00AM', '11:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (3, 6, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (4, 0, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (4, 1, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (4, 2, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (4, 3, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (4, 4, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (4, 5, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (4, 6, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (5, 0, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (5, 1, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (5, 2, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (5, 3, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (5, 4, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (5, 5, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (5, 6, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (6, 0, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (6, 1, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (6, 2, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (6, 3, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (6, 4, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (6, 5, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (6, 6, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (7, 0, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (7, 1, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (7, 2, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (7, 3, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (7, 4, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (7, 5, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (7, 6, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (8, 0, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (8, 1, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (8, 2, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (8, 3, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (8, 4, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (8, 5, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (8, 6, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (9, 0, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (9, 1, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (9, 2, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (9, 3, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (9, 4, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (9, 5, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (9, 6, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (10, 0, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (10, 1, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (10, 2, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (10, 3, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (10, 4, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (10, 5, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (10, 6, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (11, 0, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (11, 1, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (11, 2, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (11, 3, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (11, 4, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (11, 5, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (11, 6, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (12, 0, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (12, 1, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (12, 2, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (12, 3, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (12, 4, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (12, 5, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (12, 6, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (13, 0, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (13, 1, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (13, 2, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (13, 3, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (13, 4, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (13, 5, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (13, 6, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (14, 0, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (14, 1, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (14, 2, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (14, 3, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (14, 4, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (14, 5, '11:00AM', '10:00PM');
INSERT INTO restaurant_hours (restaurant_id, day_of_week_id, opening_time, closing_time) VALUES (14, 6, '11:00AM', '10:00PM');

INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (1, 0, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (1, 1, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (1, 2, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (1, 3, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (1, 4, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (1, 5, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (1, 6, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (2, 0, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (2, 1, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (2, 2, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (2, 3, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (2, 4, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (2, 5, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (2, 6, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (3, 0, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (3, 1, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (3, 2, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (3, 3, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (3, 4, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (3, 5, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (3, 6, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (4, 0, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (4, 1, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (4, 2, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (4, 3, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (4, 4, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (4, 5, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (4, 6, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (5, 0, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (5, 1, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (5, 2, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (5, 3, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (5, 4, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (5, 5, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (5, 6, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (6, 0, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (6, 1, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (6, 2, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (6, 3, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (6, 4, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (6, 5, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (6, 6, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (7, 0, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (7, 1, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (7, 2, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (7, 3, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (7, 4, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (7, 5, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (7, 6, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (8, 0, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (8, 1, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (8, 2, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (8, 3, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (8, 4, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (8, 5, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (8, 6, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (9, 0, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (9, 1, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (9, 2, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (9, 3, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (9, 4, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (9, 5, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (9, 6, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (10, 0, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (10, 1, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (10, 2, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (10, 3, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (10, 4, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (10, 5, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (10, 6, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (11, 0, '4:00PM', '8:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (11, 1, '4:00PM', '8:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (11, 2, '4:00PM', '8:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (11, 3, '4:00PM', '8:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (11, 4, '4:00PM', '8:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (11, 5, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (11, 6, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (12, 0, '4:00PM', '8:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (12, 1, '4:00PM', '8:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (12, 2, '4:00PM', '8:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (12, 3, '4:00PM', '8:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (12, 4, '4:00PM', '8:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (12, 5, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (12, 6, NULL, NULL);
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (13, 0, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (13, 1, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (13, 2, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (13, 3, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (13, 4, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (13, 5, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (13, 6, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (14, 0, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (14, 1, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (14, 2, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (14, 3, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (14, 4, '3:00PM', '6:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (14, 5, '4:00PM', '7:00PM');
INSERT INTO happy_hour_hours (restaurant_id, day_of_week_id, start_time, end_time) VALUES (14, 6, '4:00PM', '7:00PM');

INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (3, 4, 'This is great. I love it. It makes me really happy.', false, 1, 1);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 2, 'This is not bad, but it definitely could be better.', false, 2, 1);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (3, 4, 'This was amazing! Absolutely terrific, especially the happy hour prices.', false, 3, 1);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 2, 'This place was great, but I was not that impressed by the happy hour.', false, 4, 1);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (1, 1, 'This was terrible! Do not go here, ever!', false, 4, 2);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (3, 4, 'They had a great happy hour, with impressive discounts and a fun atmosphere.', false, 3, 2);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (2, 3, 'This was definitely pretty good, but not great.', false, 2, 2);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (2, 2, 'Everything about this was totally average. But I like average.', false, 1, 3);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 2, 'I really like this place, though the happy hour could have been better.', false, 1, 3);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (1, 4, 'Happy hour values were great, but this restaurant was filthy.', false, 2, 3);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 3, 'Best restaurant ever!', false, 3, 4);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (3, 3, 'This was pretty good. I like food.', false, 4, 4);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (3, 4, 'This is great. I love it. It makes me really happy.', false, 1, 4);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 2, 'This is not bad, but it definitely could be better.', false, 2, 5);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (3, 4, 'This was amazing! Absolutely terrific, especially the happy hour prices.', false, 3, 5);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 2, 'This place was great, but I was not that impressed by the happy hour.', false, 4, 5);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (1, 1, 'This was terrible! Do not go here, ever!', false, 4, 6);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (3, 4, 'They had a great happy hour, with impressive discounts and a fun atmosphere.', false, 3, 6);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (2, 3, 'This was definitely pretty good, but not great.', false, 2, 6);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (2, 2, 'Everything about this was totally average. But I like average.', false, 1, 7);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 2, 'I really like this place, though the happy hour could have been better.', false, 1, 7);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (1, 4, 'Happy hour values were great, but this restaurant was trash.', false, 2, 8);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 3, 'Best restaurant ever!', false, 3, 8);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (3, 3, 'This was pretty good. I like food.', false, 4, 9);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (1, 4, 'Happy hour values were great, but this restaurant was trash.', false, 1, 9);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 3, 'Best restaurant ever!', false, 2, 10);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 2, 'These video games are amazing!', false, 3, 5);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (1, 1, 'This was terrible! Do not go here, ever!', false, 4, 11);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (3, 4, 'They had a great happy hour, with impressive discounts and a fun atmosphere.', false, 1, 11);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (2, 3, 'This was definitely pretty good, but not great.', false, 2, 12);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (2, 2, 'Everything about this was totally average. But I like average.', false, 3, 12);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 2, 'I really like this place, though the happy hour could have been better.', false, 4, 12);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (1, 4, 'Happy hour values were great, but this restaurant was trash.', false, 1, 13);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 3, 'Best restaurant ever!', false, 2, 13);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (3, 3, 'This was pretty good. I like food.', false, 3, 13);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (1, 4, 'Happy hour values were great, but this restaurant was trash.', false, 2, 14);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 3, 'Best restaurant ever!', false, 1, 14);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (3, 4, 'They had a great happy hour, with impressive discounts and a fun atmosphere.', false, 1, 14);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (2, 3, 'This was definitely pretty good, but not great.', false, 2, 13);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (2, 2, 'Everything about this was totally average. But I like average.', false, 3, 12);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 2, 'I really like this place, though the happy hour could have been better.', false, 4, 11);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (1, 4, 'Happy hour values were great, but this restaurant was trash.', false, 1, 11);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 3, 'Best restaurant ever!', false, 2, 12);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (3, 3, 'This was pretty good. I like food.', false, 3, 10);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (1, 4, 'Happy hour values were great, but this restaurant was trash.', false, 2, 10);
INSERT INTO review (restaurant_rating, happy_hour_rating, review_text, flagged, customer_id, restaurant_id) 
VALUES (4, 3, 'Second-best restaurant ever!', false, 1, 14);

COMMIT;