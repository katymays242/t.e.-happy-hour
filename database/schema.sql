-- *************************************************************************************************
-- This script creates all of the database objects (tables, sequences, etc) for the database
-- *************************************************************************************************

BEGIN;

CREATE TABLE account
(
account_id int NOT NULL,
account_type VARCHAR(100) NOT NULL,

CONSTRAINT pk_account_id PRIMARY KEY (account_id)
);

CREATE TABLE customer
(
customer_id SERIAL NOT NULL,
first_name VARCHAR(64) NOT NULL,
last_name VARCHAR(64) NOT NULL,
email VARCHAR(120) NOT NULL,
password VARCHAR(120) NOT NULL,
salt varchar(256) NOT NULL,
account_id int DEFAULT 1,

CONSTRAINT pk_customer_id PRIMARY KEY (customer_id),
CONSTRAINT fk_customer_id_account_id FOREIGN KEY (account_id) REFERENCES account(account_id)
);

CREATE TABLE restaurant
(
restaurant_id SERIAL NOT NULL,
restaurant_name VARCHAR(150) NOT NULL,
addressLine1 VARCHAR(200) NOT NULL,
addressLine2 VARCHAR(200) NULL,
city VARCHAR(100) NOT NULL,
state VARCHAR(50) NOT NULL,
postalCode VARCHAR(10) NOT NULL,
restaurant_phone_number VARCHAR(20) NOT NULL,
ownerVerified BOOLEAN DEFAULT FALSE,
websiteLink VARCHAR(255) NULL,
restaurantimage VARCHAR(50) DEFAULT 'default.jpg',
restaurant_text text NULL,
happyhour_text text NULL,
specials text NULL,

CONSTRAINT pk_restaurant_id PRIMARY KEY (restaurant_id)
);

CREATE TABLE neighborhood
(
neighborhood_id SERIAL NOT NULL,
name VARCHAR(255) NOT NULL,

CONSTRAINT pk_neighborhood_id PRIMARY KEY (neighborhood_id)
);

CREATE TABLE customer_neighborhood
(
customer_id int NOT NULL,
neighborhood_id int NOT NULL,

CONSTRAINT pk_customer_neighborhood_customer_id_neighborhood_id PRIMARY KEY (customer_id, neighborhood_id),
CONSTRAINT fk_customer_neighborhood_customer_id FOREIGN KEY (customer_id) REFERENCES customer(customer_id),
CONSTRAINT fk_customer_neighborhood_neighborhood_id FOREIGN KEY (neighborhood_id) REFERENCES neighborhood(neighborhood_id)
);

CREATE TABLE food_type
(
food_type_id SERIAL NOT NULL,
food_type_name VARCHAR(120) NOT NULL,

CONSTRAINT pk_food_type_id PRIMARY KEY (food_type_id)
);

CREATE TABLE customer_food_type
(
customer_id int NOT NULL,
food_type_id int NOT NULL,

CONSTRAINT pk_customer_food_type_customer_id_food_type_id PRIMARY KEY (customer_id, food_type_id),
CONSTRAINT fk_customer_food_type_customer_id FOREIGN KEY (customer_id) REFERENCES customer(customer_id),
CONSTRAINT fk_customer_food_type_food_type_id FOREIGN KEY (food_type_id) REFERENCES food_type(food_type_id)
);

CREATE TABLE price
(
price_id SERIAL NOT NULL,
price_range VARCHAR(50) NOT NULL,

CONSTRAINT pk_price_id PRIMARY KEY (price_id)
);

CREATE TABLE day_of_week
(
day_of_week_id SERIAL NOT NULL,
day_of_week VARCHAR NOT NULL,

CONSTRAINT pk_day_of_week_id PRIMARY KEY (day_of_week_id)
);

CREATE TABLE customer_price
(
customer_id int NOT NULL,
price_id int NOT NULL,

CONSTRAINT pk_customer_price_customer_id_price_id PRIMARY KEY (customer_id, price_id),
CONSTRAINT fk_customer_price_customer_id FOREIGN KEY (customer_id) REFERENCES customer(customer_id),
CONSTRAINT fk_customer_price_price_id FOREIGN KEY (price_id) REFERENCES price(price_id)
);

CREATE TABLE restaurant_price
(
restaurant_id int NOT NULL,
price_id int NOT NULL,

CONSTRAINT pk_restaurant_price_restaurant_id_price_id PRIMARY KEY (restaurant_id, price_id),
CONSTRAINT fk_restaurant_price_restaurant_id FOREIGN KEY (restaurant_id) REFERENCES restaurant(restaurant_id),
CONSTRAINT fk_restaurant_price_price_id FOREIGN KEY (price_id) REFERENCES price(price_id)
);

CREATE TABLE ownership
(
ownership_id SERIAL NOT NULL,
restaurant_id int NOT NULL,
customer_id int NOT NULL,

CONSTRAINT pk_ownership_ownership_id PRIMARY KEY (ownership_id),
CONSTRAINT fk_ownership_customer_id FOREIGN KEY (customer_id) REFERENCES customer(customer_id),
CONSTRAINT fk_ownership_restaurant_id FOREIGN KEY (restaurant_id) REFERENCES restaurant(restaurant_id)
);

CREATE TABLE pending_owners
(
submission_id SERIAL NOT NULL,
customer_id int NOT NULL,
restaurant_name VARCHAR(150) NOT NULL,
neighborhood_id int NOT NULL,
restaurant_phone_number VARCHAR(20) NOT NULL,
approval_date DATE NULL,
approval_status VARCHAR(50) DEFAULT 'pending',

CONSTRAINT pk_submission_id PRIMARY KEY (submission_id),
CONSTRAINT fk_pending_owners_customer_id FOREIGN KEY (customer_id) REFERENCES customer(customer_id)
);

CREATE TABLE restaurant_hours
(    
restaurant_id int NOT NULL,
day_of_week_id int NOT NULL,
opening_time VARCHAR(15) NULL,
closing_time VARCHAR(15) NULL,

CONSTRAINT pk_restaurant_hours_restaurant_id_day_of_week PRIMARY KEY (restaurant_id, day_of_week_id),
CONSTRAINT fk_restaurant_hours_restaurant_id FOREIGN KEY (restaurant_id) REFERENCES restaurant(restaurant_id)
);

CREATE TABLE happy_hour_hours
(  
restaurant_id int NOT NULL,
day_of_week_id int NOT NULL,
start_time VARCHAR(15) NULL,
end_time VARCHAR(15) NULL,

CONSTRAINT pk_happy_hour_hours_restaurant_id_day_of_week PRIMARY KEY (restaurant_id, day_of_week_id),
CONSTRAINT fk_happy_hour_hours_restaurant_id FOREIGN KEY (restaurant_id) REFERENCES restaurant(restaurant_id)
);

CREATE TABLE restaurant_neighborhood
(
restaurant_id int NOT NULL,
neighborhood_id int NOT NULL,

CONSTRAINT pk_restaurant_neighborhood_neighborhood_id_restaurant_id PRIMARY KEY (restaurant_id, neighborhood_id),
CONSTRAINT fk_restaurant_neighborhood_neighborhood_id FOREIGN KEY (neighborhood_id) REFERENCES neighborhood(neighborhood_id),
CONSTRAINT fk_restaurant_neighborhood_restaurant_id FOREIGN KEY (restaurant_id) REFERENCES restaurant(restaurant_id)
);

CREATE TABLE restaurant_food_type
(
restaurant_id int NOT NULL,
food_type_id int NOT NULL,

CONSTRAINT pk_restaurant_food_type_food_type_id_restaurant_id PRIMARY KEY (food_type_id, restaurant_id),
CONSTRAINT fk_restaurant_food_type_food_type_id FOREIGN KEY (food_type_id) REFERENCES food_type(food_type_id),
CONSTRAINT fk_restaurant_food_type_restaurant_id FOREIGN KEY (restaurant_id) REFERENCES restaurant(restaurant_id)
);


CREATE table review
(
review_id SERIAL NOT NULL,
restaurant_rating int NULL,
happy_hour_rating int NOT NULL,
review_text text NULL,
flagged BOOLEAN DEFAULT false,
customer_id int NOT NULL,
restaurant_id int NOT NULL,

CONSTRAINT pk_review_review_id PRIMARY KEY (review_id),
CONSTRAINT fk_review_customer_id FOREIGN KEY (customer_id) REFERENCES customer(customer_id),
CONSTRAINT fk_review_restaurant_id FOREIGN KEY (restaurant_id) REFERENCES restaurant(restaurant_id)
);



COMMIT;