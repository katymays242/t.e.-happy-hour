<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

<div id="divdeps" style="display:none" title=""></div>

<section id="main-content">
<div class="row">  
      
<c:forEach var="restaurant" items="${restaurantListNeighborhood}">
 	<div class="col-sm-6">
		<c:url var="restaurantLink" value="/restaurantDetail">
		<c:param name="restaurantId" value="${restaurant.restaurantId}"></c:param>
		</c:url>
		<div id= "tilesImg"><a href="${restaurantLink}"><img src="img/${restaurant.restaurantImage}" height="300" width="400" class="images"></a></div>
		
		<div id="restaurantName"><p><c:out value="${restaurant.restaurantName}"/></p></div>
		<div class = "dollarSigns">
					<img src="img/dollar${restaurant.priceRange}.png" style= "width: 9em; padding-top: 0;" />
		</div>
	</div>
</c:forEach>
</div>
</section>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />