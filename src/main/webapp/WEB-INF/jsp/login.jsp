<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<script type="text/javascript">
	$(document).ready(function () {
	
		$("form").validate({
			
			rules : {
				email : {
					required : true
				},
				password : {
					required : true
				}
			},
			errorClass : "error"
		});
	});
</script>

<div class="row">
	<div class="col-xs-3 col-sm-4"></div>
	<div class="col-xs-6 col-sm-4">
		<c:url var="formAction" value="/login" />
		<form method="POST" action="${formAction}">
			<div id="formbox"><div class="form-group">
				<label for="email">Email: </label>
				<input type="text" id="email" name="email" placeHolder="Email" class="form-control" />
			</div>
			<div class="form-group">
				<label for="password">Password: </label>
				<input type="password" id="password" name="password" placeHolder="Password" class="form-control" />
			</div>
			 <input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}">
			 <input type="hidden" name="destination" value="${param.destination}">
			<button type="submit" class="btn btn-default" id="btn_l">Login</button>
		</div>
		</form>
	</div>
	<div class="col-sm-4"></div>
</div>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />







