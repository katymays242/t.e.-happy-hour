<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

<section id="main-content">
	<div class="row">
		<section id="pending-content">
			<h1>Pending Owners (<c:out value="${pendingList.size()}" />)</h1>
			<c:set var="count" value="1" scope="page" />
			<c:forEach var="pendingResults" items="${pendingList}">
				<div class="col-sm-6">
					<div id="ownerPending">
						<p>(${count})</p>
						<c:set var="count" value="${count + 1}" scope="page"/>
						<div id="adminRestaurantName">
							<p>
								Restaurant Name:
								<c:out value="${pendingResults.restaurantName}" />
							</p>
						</div>
						<div id="adminneighborhood">
							<p>
								Restaurant Neighborhood:
								<c:out value="${pendingResults.neighborhood_id}" />
							</p>
						</div>
						<div id="adminApprovalStatus">
							<p>
								Current Status:
								<c:out value="${pendingResults.approval_status}" />
							</p>
						</div>
						<div id="adminCustomerName">
							<p>
								Pending Owner Name:
								<c:out value="${pendingResults.user.firstName}" />
								<c:out value="${pendingResults.user.lastName}" />
							</p>
						</div>
						<c:url var="formAction" value="/pendingOwner" />
							<form method="POST" action="${formAction}">
							<div id="adminCustomerName">
								<strong>
									<p>Is
										<c:out value="${pendingResults.user.firstName}" />
										<c:out value="${pendingResults.user.lastName}" />
									</p>
								the owner of
							<c:out value="${pendingResults.restaurantName}" />
							</strong>
							<button name="approval" type="submit" value="approved">Yes</button>
							<button name="approval" type="submit" value="rejected">No</button>
							<input type="hidden" name="id" value="${pendingResults.submission_id}" />
							<input type="hidden" name="userId" value="${pendingResults.user.id}" />
	<%-- 						<input type="hidden" name="restaurantId" value="${pendingResults.restaurant.restaurantId}" />  --%>
							</div>
						</form>
					</div>
				</div>
			</c:forEach>
		</section>
	</div>
</section>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />