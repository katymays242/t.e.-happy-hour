<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

<p>Don't see your favorite restaurant in our database? Fill out the fields below:</p>

<script type="text/javascript">		
$(document).ready(function () {	
		$("form").validate({

			debug: false,
			rules : {

				restaurantName : {
					required : true
				},

				restaurantAddress : {
					required : true
				},
				
				restaurantAddress2 : {
					required : false
				},
				
				restaurantCity : {
					required : true
				},
				
				restaurantState : {
					required : true
				},
				
				restaurantZipCode : {
					required : true,
					minlength: 5
				},
				
				websiteLink : {
					required : false,
					url: true
				},
				
				phoneNumber : {
					required : true,
					phoneFormat: true
				}
			},
			messages: {
				restaurantZipCode: {
	                minlength: "Zip code must be 5 characters!"
	            },
	            phoneNumber: {
	                required: "Phone number is required!"
	            }
	        },
			
			errorClass: "error",
			validClass: "valid"
		});
		
		$('.timepicker').timepicker({
 			noneOption:
	        {
	            'label': 'Closed',
	            'value': '0:00 PM'
	        },
		    timeFormat: 'h:i A',
		    interval: 60,
		    minTime: '6:00am',
		    interval: 30,
		    scrollDefault: true
		  
		});
		
		$('.timepicker2').timepicker({
 			noneOption:
	        {
	            'label': 'Closed',
	            'value': '0:00 PM'
	        },
		    timeFormat: 'h:i A',
		    interval: 60,
		    minTime: '8:00pm',
		    interval: 30,
		    scrollDefault: true
		  
		});
		
		$('.timepickerHH').timepicker({
			noneOption:
		        {
		            'label': 'None',
		            'value': '0:00 PM'
		        },
		    timeFormat: 'h:i A',
		    interval: 60,
		    minTime: '3:00pm',
		    interval: 30,
		    scrollDefault: true
		}); 
		
		$('.timepickerHH2').timepicker({
			noneOption:
		        {
		            'label': 'None',
		            'value': '0:00 PM'
		        },
		    timeFormat: 'h:i A',
		    interval: 60,
		    minTime: '6:00pm',
		    interval: 30,
		    scrollDefault: true
		}); 
});

/* $.validator.addMethod("phoneFormat", function (value, index) {
    return value.match(^[2-9]\d{2}-\d{3}-\d{4}$);
}, "Please enter a valid phone number: XXX-XXX-XXXX"); */
</script>

<c:url var="formAction" value="/submitRestaurant" />
<form:form method="POST" action="${formAction}" modelAttribute="newRestaurant">
	<div class="row">
		<div class="col-sm-3"></div>
		<div class="col-sm-6">
			<div class="form-group">
				<form:label path="restaurantName">Restaurant Name: </form:label>
				<form:input type="text" id="restaurantName" path="restaurantName" placeHolder="Restaurant Name" class="form-control" />
				<form:errors path="restaurantName" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="restaurantAddress">Restaurant Address: </form:label>
				<form:input type="text" id="restaurantAddress" path="restaurantAddress" placeHolder="Restaurant Address" class="form-control" />
				<form:errors path="restaurantAddress" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="restaurantAddress2">Restaurant Address: Line 2 (if necessary) </form:label>
				<form:input type="text" id="restaurantAddress2" path="restaurantAddress2" placeHolder="Restaurant Address 2" class="form-control" />
				<form:errors path="restaurantAddress2" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="restaurantCity">City: </form:label>
				<form:input type="text" id="restaurantCity" path="restaurantCity" placeHolder="City" class="form-inline" />
				<form:errors path="restaurantCity" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="restaurantState">State: </form:label>
				<form:input type="text" id="restaurantState" path="restaurantState" placeHolder="State" class="form-inline" />
				<form:errors path="restaurantState" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="restaurantZipCode">Zip Code: </form:label>
				<form:input type="text" id="restaurantZipCode" path="restaurantZipCode" placeHolder="Zip Code" class="form-inline" />
				<form:errors path="restaurantZipCode" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="phoneNumber">Phone Number: </form:label>
				<form:input type="text" id="phoneNumber" path="phoneNumber" placeHolder="Phone Number" class="form-inline" />
				<form:errors path="phoneNumber" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="neighborhoodId">Neighborhood: </form:label>
				<form:select path="neighborhoodId">
					<form:option value="1">Campus</form:option>
					<form:option value="2">Bexley</form:option>
					<form:option value="3">Gahanna</form:option>
					<form:option value="4">Downtown</form:option>
					<form:option value="5">Easton</form:option>
					<form:option value="6">Short North</form:option>
					<form:option value="7">Grandview</form:option>
					<form:option value="8">Clintonville</form:option>
					<form:option value="9">Upper Arlington</form:option>
					<form:option value="10">Italian Village</form:option>
					<form:option value="11">German Village</form:option>
					<form:option value="12">Victorian Village</form:option>
					<form:option value="13">Brewery District</form:option>
					<form:option value="14">Arena District</form:option>
					<form:option value="15">Dublin</form:option>
				</form:select>
				<form:errors path="neighborhoodId" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="priceRange">Price Range: </form:label>
				<form:select path="priceRange">
					<form:option value="1">$ ($0-$20)</form:option>
					<form:option value="2">$$ ($21-$50)</form:option>
					<form:option value="3">$$$ ($51-$100)</form:option>
					<form:option value="4">$$$$ ($101 and up)</form:option>
				</form:select>
				<form:errors path="priceRange" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="typeOfFood">Type of Food: </form:label>
				<form:select path="typeOfFood">
					<form:option value="1">American</form:option>
					<form:option value="2">African</form:option>
					<form:option value="3">Asian</form:option>
					<form:option value="4">BBQ</form:option>
					<form:option value="5">Bar Food</form:option>
					<form:option value="6">Breakfast</form:option>
					<form:option value="7">Burger</form:option>
					<form:option value="8">Cafe</form:option>
					<form:option value="9">Caribbean</form:option>
					<form:option value="10">Chinese</form:option>
					<form:option value="11">Coffee and Tea</form:option>
					<form:option value="12">Diner</form:option>
					<form:option value="13">Drinks Only</form:option>
					<form:option value="14">European</form:option>
					<form:option value="15">Greek</form:option>
					<form:option value="16">Indian</form:option>
					<form:option value="17">Italian</form:option>
					<form:option value="18">Japanese</form:option>
					<form:option value="19">Latin American</form:option>
					<form:option value="20">Mexican</form:option>
					<form:option value="21">Middle Eastern</form:option>
					<form:option value="22">Pizza</form:option>
					<form:option value="23">Sandwich</form:option>
					<form:option value="24">Seafood</form:option>
					<form:option value="25">Soul Food</form:option>
					<form:option value="26">Steak</form:option>
					<form:option value="27">Sushi</form:option>
					<form:option value="28">Taco</form:option>
					<form:option value="29">Thai</form:option>
					<form:option value="30">Vegetarian</form:option>
				</form:select>
				<form:errors path="typeOfFood" cssClass="error" />
			</div>

			<div class="form-group">
				<form:label path="websiteLink">Website Link (optional): </form:label>
				<form:input type="text" id="websiteLink" path="websiteLink"
					placeHolder="Website Link" class="form-control" />
				<form:errors path="websiteLink" cssClass="error" />
			</div>
			<br>
			<div class="form-group">
				<p>
					<b>What are your restaurant's hours?</b>
				</p>
				<c:set var="daysOfWeek" value="${fn:split('Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday', ',')}"/>
				<table>
				<c:forEach var="dayOfWeek" items="${daysOfWeek}">
					<tr>
 					<th><label>${dayOfWeek}:</label></th>
					<td><input name="${dayOfWeek}OpeningTime" class="timepicker text-center"></td>
					<td>to</td>
					<td><input name="${dayOfWeek}ClosingTime" class="timepicker2 text-center"></td>
					</tr>
				</c:forEach>
				</table>
				</div>
				<br>
				<div class="form-group">
				<form:label path="restaurantText">Describe your restaurant in one sentence: </form:label>
				<form:input type="text" id="restaurantText" path="restaurantText"
					placeHolder="Describe your restaurant" class="form-control" />
				<form:errors path="restaurantText" cssClass="error" />
				</div>
				<br>
				<div class="form-group">
				<p>
					<b>What are your restaurant's Happy Hour times?</b>
				</p>
				<table>
				<c:forEach var="dayOfWeek" items="${daysOfWeek}">
					<tr>
					<th><label>${dayOfWeek}:</label></th>
					<td><input name="${dayOfWeek}HappyHourStart" class="timepickerHH text-center"></td>
					<td>to</td>
					<td><input name="${dayOfWeek}HappyHourEnd" class="timepickerHH2 text-center"></td>
					</tr>
				</c:forEach>
				</table>
				<br>
				<div class="form-group">
				<form:label path="happyHourText">Describe your Happy Hour specials: </form:label>
				<form:input type="text" id="happyHourText" path="happyHourText"
					placeHolder="Describe your Happy Hour" class="form-control" />
				<form:errors path="happyHourText" cssClass="error" />
				</div>
				<br>
				<div class="form-group">
				<form:label path="specials">Have any special offers or coupons for Columbus Happy Hour users? </form:label>
				<form:input type="text" id="specials" path="specials"
					placeHolder="Specials or coupons" class="form-control" />
				<form:errors path="happyHourText" cssClass="error" />
				</div>
				<br>
			</div>
			<button type="submit" class="btn btn-default" id="submitButton">Submit
				Happy Hour</button>
		</div>
		<div class="col-sm-4"></div>
	</div>
	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}">
</form:form>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />