<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Columbus Happy Hour</title>
		<meta charset="utf-8">
      	<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Glegoo" rel="stylesheet">
		<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	    <script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
	    <script src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.js "></script>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.css">
	    <script src="https://cdn.jsdelivr.net/jquery.timeago/1.4.1/jquery.timeago.min.js"></script>
	    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	    <c:url var="cssUrl" value="/css/site.css" />
	 	<link rel="stylesheet" type="text/css" href="${cssUrl}">
		
		<script type="text/javascript">
			$(document).ready(function(){
				$("#logoutLink").click(function(event){
					$("#logoutForm").submit();
					return false;
				});
			});
		</script>
	</head>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.min.js"></script>
	
	<body>
	<div class="container"> 
		<header>
			<c:url var="homePageHref" value="/" />
			<c:url var="imgSrc" value="/img/logo.png" />
			<c:url var ="submitNewRestaurant" value="/submitRestaurant" />
			<c:url var ="viewByPrices" value="/prices" />
			<c:url var ="viewByRatings" value="/ratings" />
			
			<c:url var ="viewShortNorth" value="/neighborhood">
			<c:param name="neighborhood" value="Short North"/>
			</c:url>
			
			<c:url var ="viewGrandview" value="/neighborhood">
			<c:param name="neighborhood" value="Grandview"/>
			</c:url>
			
			<c:url var ="viewArenaDistrict" value="/neighborhood">
			<c:param name="neighborhood" value="Arena District"/>
			</c:url>
			
			
			<a href="${homePageHref}"><img id="header-image" src="${imgSrc}" class="img-responsive" /></a>

			<!-- Navigation -->
			<script>
				$(document).ready(function(){
					var $attribute = $('[data-smart-affix]');
					$attribute.each(function(){
					  $(this).affix({
					    offset: {
					      top: $(this).offset().top
					    }
					  })
					})
					$(window).on("resize", function(){
					  $attribute.each(function(){
					    $(this).data('bs.affix').options.offset = $(this).offset().top
					  })
					})
					
					$(window).scroll(function(event) {
						console.log($(window).scrollTop());
					});
				});
			</script>
			<nav class="navbar navbar-default" data-spy="affix"
				data-smart-affix="245">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"
						aria-expanded="false">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="${homePageHref}"><span style="font-size: 1em" class="glyphicon glyphicon-home"></span>Home</a></li>
						<li><a href="${submitNewRestaurant}"><span style="font-size: 1em" class="glyphicon glyphicon-pencil"></span>Submit a Happy Hour</a></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span style="font-size: 1em" class="glyphicon glyphicon-globe"></span> Neighborhood<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="${viewShortNorth}">Short North</a></li>
								<li><a href="${viewGrandview}">Grandview</a></li>
								<li><a href="${viewArenaDistrict}">Arena District</a></li>
							</ul>
						</li>
						<li>
							<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span style="font-size: 1em" class="glyphicon glyphicon-sort-by-attributes-alt"></span> Sort By<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="${viewByRatings}"">Top-rated Happy Hours</a></li>
								<li><a href="${viewByPrices}">Price</a></li>
							</ul>
						</li>
						<li>
						<c:choose>
							<c:when test="${empty email}">
								<c:url var="newUserHref" value="register" />
								<li><a href="${newUserHref}"><span style="font-size: 1em" class="glyphicon glyphicon-plus-sign"></span> Sign Up</a></li>
								<c:url var="loginHref" value="login" />
								<li><a href="${loginHref}"><span style="font-size: 1em" class="glyphicon glyphicon-user"></span> Log In</a></li>
							</c:when>
							<c:otherwise>
								<c:url var="logoutAction" value="/logout" />
								<form id="logoutForm" action="${logoutAction}" method="POST">
									<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}">
								</form>
								<li><a id="logoutLink" href="#"><c:if test="${not empty email}">Log Out User: ${email}</c:if>	
		</a></li>
		<%-- <c:if test="${not empty email}">
			<p id="currentUser">User: ${email}</p>
		</c:if>	
		</a></li> --%>
							</c:otherwise>
						</c:choose>
						</li>
					</ul>
					<div class="col-sm-3 col-md-3 pull-right">
						<div class="navbar-form" role="search">
							<div class="input-group">
							<c:url var="searchAction" value="/searchResults" />
							<form:form method="POST" action="${searchAction}"> 
								<input type="text" class="form-control" name="srchterm" placeholder="Search" id="srch-term"/>
								<div class="input-group-btn">
									<button type="submit" class="btn btn-default" id="searchSubmitButton">
										<i class="glyphicon glyphicon-search"></i>
									</button>
								</div>
						</form:form>
								</div>
							</div>		
					</div>
				</div>
			</nav>
		</header>