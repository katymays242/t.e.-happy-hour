<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
	<c:url var="homePageHref" value="/" />
	<c:url var="imgSrc" value="/img/Thank-You.png" />
	<div id="thankYou"><img src="${imgSrc}" class="img-responsive1" />
	<p>Someone will be in touch with you shortly to verify your business</p>
	</div>
<c:import url="/WEB-INF/jsp/common/footer.jsp" />