<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />
<script>
	$(document).ready(function() {
		$("#showReviewForm").click(function() {
			$("#reviewForm").toggle();
		});
	});
</script>

<div class="jumbotron jumbotron-billboard">
	<div class="img"></div>
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div id="restaurantName">
					<p>
						<c:out value="${restaurant.restaurantName}" />
					</p>
				</div>
				<div class = "dollarSigns">
					<img src="img/dollar${restaurant.priceRange}.png" style="width: 9em; height: 1.3em;" />
				</div>
				<div class="restaurantDetails">
					<p>
						<c:out value="${restaurant.typeOfFoodString}" />
					</p>
				</div>
				<div class="restaurantDetails">
					<p>
						<c:out value="${restaurant.neighborhood}" />
					</p>
				</div>
				<div class="restaurantDetails">
					<p>
						<c:out value="${restaurant.phoneNumber}" />
					</p>
				</div>
				<div class="restaurantDetails">
					<p>
						<c:out value="${restaurant.restaurantAddress}" />
					</p>
				</div>
				<div class="restaurantDetails">
					<p>
						<c:out value="${restaurant.restaurantCity}" />, <c:out value="${restaurant.restaurantState}" /> <c:out value="${restaurant.restaurantZipCode}" />
					</p>
				</div>
				<div class="restaurantDetails">
					<a href = <c:out value="${restaurant.websiteLink}" />><c:out value="${restaurant.websiteLink}"/></a>
				</div>
			</div>
			<div class="col-lg-6">
				<div id="imageDetails"></div>
				<img src="img/${restaurant.restaurantImage}" height="300"
					width="500">
			</div>
		</div>
	</div>
	<div class = "allRestaurantText">
			<div class="restaurantText">
				<p>
					<c:out value="${restaurant.restaurantText}" />
				</p>
			</div>
			<div class="restaurantText">
				<p>
					<c:out value="${restaurant.happyHourText}" />
				</p>
			</div>
			<div class="restaurantText">
				<p>
					<c:out value="${restaurant.specials}" />
				</p>
			</div>
	</div>
</div>
<button type="button"
	class="btn btn-secondary btn-lg btn-block customButton"
	style="display: block; margin: 0 auto;" id="showReviewForm">Rate
	and Review</button>
<div id="reviewForm" style="display: none;">
	<c:url var="formAction" value="/submitReview" />
	<form:form method="POST" action="${formAction}"> 
	<form class="form-horizontal">
	<div id="starsContainer">
	<label class="control-label col-sm-2" for="message">Rate This Restaurant:</label>
		<div class="stars">
			<input class="star star-5" id="star-5" type="radio" value="5" name="star" /> <label
				class="star star-5" for="star-5"></label> 
			<input class="star star-4" id="star-4" type="radio" value="4" name="star" /> <label
				class="star star-4" for="star-4"></label>
			<input class="star star-3" id="star-3" type="radio" value="3" name="star" /> <label 
				class="star star-3" for="star-3"></label> 
			<input class="star star-2" id="star-2" type="radio" value="2" name="star" /> <label 
				class="star star-2" for="star-2"></label> 
			<input class="star star-1" id="star-1" type="radio" value="1" name="star" /> <label 
				class="star star-1" for="star-1"></label> 
		</div>
		</div>
	<div id="martiniContainer">	
	<label class="control-label col-sm-2" for="message">How good is the happy hour? </label>
		<div class="martinis" id="martinisDiv">
			<input class="martini martini-5" id="martini-5" type="radio" value="5" name="martini" /> <label
				class="martini martini-5" for="martini-5"></label> 
			<input class="martini martini-4" id="martini-4" type="radio" value="4" name="martini" /> <label
				class="martini martini-4" for="martini-4"></label>
			<input class="martini martini-3" id="martini-3" type="radio" value="3" name="martini" /> <label 
				class="martini martini-3" for="martini-3"></label> 
			<input class="martini martini-2" id="martini-2" type="radio" value="2" name="martini" /> <label 
				class="martini martini-2" for="martini-2"></label> 
			<input class="martini martini-1" id="martini-1" type="radio" value="1" name="martini" /> <label 
				class="martini martini-1" for="martini-1"></label> 
		</div>
		</div>
		<div id="reviewTextContainer">
		<div class="form-group">
			<label class="control-label col-sm-2" for="message">Review: </label>
			<div class="col-sm-10">
				<textarea class="form-control" rows="5" id="message" name="reviewText"></textarea>
			</div>
		</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10"></div>
		</div>
		<br>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default">Post Review</button>
				<input type ="hidden" name ="restaurantId" value="${restaurant.restaurantId}"/>
			</div>
		</div>
</form>
</form:form>
</div>
<section id="main-content">
	<div id="review">
		<c:forEach var="review" items="${reviewList}">
		<div class="ratings">
		<div><img src="img/${review.restaurantRating}-star.png" style="width: 9em; height: 1.3em;" /></div>
		<div><img src="img/martini-${review.happyHourRating}.png" style="width: 9em; height: 1.3em;" /></div>
		</div>
		<div class="reviewText">
			<p>${review.reviewText}</p>
			<hr>
		</div>
			
		</c:forEach>
	</div>


</section>

<c:import url="/WEB-INF/jsp/common/footer.jsp" />