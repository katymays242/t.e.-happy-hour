<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<c:import url="/WEB-INF/jsp/common/header.jsp" />

<script type="text/javascript">
	$(document).ready(function() {
		$('#checkbox').click(function() {
			if ($('#checkbox').is(':checked')) {
				$("#owner").show();
			} else {
				$("#owner").hide();
			}
		});

		$("#user-form").validate({

			rules : {

				firstName : {
					required : true
				},

				lastName : {
					required : true
				},

				email : {
					required : true
				},
				password : {
					required : true
				},
				confirmPassword : {
					required : true,
					equalTo : "#password"
				}
			},
			messages : {
				confirmPassword : {
					equalTo : "Passwords do not match"
				}
			},

			errorClass : "error"
		});
	});
</script>

<c:url var="formAction" value="/submit" />
<form:form id="user-form" method="POST" action="${formAction}" modelAttribute="user">
	<div class="row">
		<div class="col-sm-4"></div>
		<div class="col-sm-4">
			<div class="form-group">
				<form:label path="firstName">First Name: </form:label>
				<form:input type="text" id="firstName" path="firstName"
					placeHolder="First Name" class="form-control" />
				<form:errors path="firstName" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="lastName">Last Name: </form:label>
				<form:input type="text" id="lastName" path="lastName"
					placeHolder="Last Name" class="form-control" />
				<form:errors path="lastName" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="email">Email: </form:label>
				<form:input type="text" id="email" path="email" placeHolder="Email"
					class="form-control" />
				<form:errors path="email" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="password">Password: </form:label>
				<br>
				<form:input type="password" id="password" path="password"
					placeHolder="Password" class="form-control" />
				<form:errors path="password" cssClass="error" />
			</div>
			<div class="form-group">
				<form:label path="confirmPassword">Confirm Password: </form:label>
				<form:input type="password" id="confirmPassword"
					path="confirmPassword" placeHolder="Re-Type Password"
					class="form-control" />
				<form:errors path="confirmPassword" cssClass="error" />
			</div>
			<div class="form-group">
				<label for="userType">Are you the owner of a restaurant? </label><br>
				<input id="checkbox" type=checkbox> Check here to get your
				restaurant verified<br>
			</div>
			<div class="form-group" id="owner" style="display: none;">
				<form:label path="restaurantName">Restaurant Name: </form:label>
				<form:input type="text" id="restaurantName" path="restaurantName"
					placeHolder="Restaurant Name" />
				<form:label path="businessPhone">Business Phone Number: </form:label>
				<form:input type="text" id="businessPhone" path="businessPhone"
					placeHolder="Business Phone Number" />
				<form:label path="neighborhood">Neighborhood: </form:label>
				<form:select path="neighborhood">
					<option value="1">Campus</option>
					<option value="2">Bexley</option>
					<option value="3">Gahanna</option>
					<option value="4">Downtown</option>
					<option value="5">Easton</option>
					<option value="6">Short North</option>
					<option value="7">Grandview</option>
					<option value="8">Clintonville</option>
					<option value="9">Upper Arlington</option>
					<option value="10">Italian Village</option>
					<option value="11">German Village</option>
					<option value="12">Victorian Village</option>
					<option value="13">Brewery District</option>
					<option value="14">Arena District</option>
					<option value="15">Dublin</option>
				</form:select>
			</div>
			<button type="submit" class="btn btn-default" id="submitButton">Create
				User</button>
		</div>
		<div class="col-sm-4"></div>
	</div>

	<input type="hidden" name="CSRF_TOKEN" value="${CSRF_TOKEN}">
</form:form>


<c:import url="/WEB-INF/jsp/common/footer.jsp" />








