package com.techelevator.model;

import java.util.List;

public interface ReviewDAO {
	public void saveReview(Review review);
	public List<Review> getReview(Restaurant restaurant);
	public List<Review> getAllReviews();
	public List<Review> getReviewsByRestaurant(Review review); 
	public long getCustomerIDByEmail(Object email);
}
