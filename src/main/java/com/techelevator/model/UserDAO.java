package com.techelevator.model;

public interface UserDAO {

	public void saveUser(User user);
	public void pendingOwner(User owner);
	public boolean searchForUsernameAndPassword(String email, String password);

}
