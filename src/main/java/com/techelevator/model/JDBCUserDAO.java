package com.techelevator.model;

import javax.sql.DataSource;

import org.bouncycastle.util.encoders.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import com.techelevator.capstone.security.PasswordHasher;


@Component
public class JDBCUserDAO implements UserDAO {
	
	private JdbcTemplate jdbcTemplate;
	private PasswordHasher passwordHasher;
	
	@Autowired
	public JDBCUserDAO (DataSource dataSource, PasswordHasher passwordHasher) {
		this.jdbcTemplate= new JdbcTemplate (dataSource);
		this.passwordHasher = passwordHasher;
	}

	
	@Override
	public void saveUser(User user) {
		long id = getNextId();
		
		
		byte[] salt = passwordHasher.generateRandomSalt();
		String hashedPassword = passwordHasher.computeHash(user.getPassword(), salt);
		String saltString = new String(Base64.encode(salt));
	
		
		String sqlInsertNewUser = "INSERT INTO customer(customer_id, email, password, first_name, last_name, salt) VALUES(?,?,?,?,?,?)";
		jdbcTemplate.update(sqlInsertNewUser, id, user.getEmail(), hashedPassword, user.getFirstName(), user.getLastName(), saltString);
		user.setId(id);
    }


	@Override
	public void pendingOwner(User owner) {
		String sqlInsertPendingOwner = "INSERT INTO pending_owners(customer_id, restaurant_name, neighborhood_id, restaurant_phone_number) VALUES(?,?,?,?)";
		jdbcTemplate.update(sqlInsertPendingOwner, owner.getId(), owner.getRestaurantName(), owner.getNeighborhood(), owner.getBusinessPhone());
		
	}

	
	
	private Long getNextId() {
		String sqlSelectNextId = "SELECT NEXTVAL('customer_customer_id_seq')";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectNextId);
		Long id = null;
		if(results.next()) {
			id = results.getLong(1);
		} else {
			throw new RuntimeException("Something strange happened, unable to select next forum post id from sequence");
		}
		return id;
	}


	@Override
	public boolean searchForUsernameAndPassword(String email, String password) {
		String sqlSearchForUser = "SELECT * "+
			      "FROM customer "+
			      "WHERE UPPER(email) = ?";

		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchForUser, email.toUpperCase());
		if(results.next()) {
			String storedSalt = results.getString("salt");
			String storedPassword = results.getString("password");
			String hashedPassword = passwordHasher.computeHash(password, Base64.decode(storedSalt));
			
			return storedPassword.equals(hashedPassword);
		} else {
			return false;
		}
	}
}

