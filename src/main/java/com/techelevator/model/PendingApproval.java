package com.techelevator.model;

import java.sql.Date;


public class PendingApproval {

	private Restaurant restaurant;
	private User user;
	private Long submission_id;
	private Long customer_id;
	private String restaurantName;
	private Long neighborhood_id;
	private String restaurant_phone_number;
	private Date approval_date;
	private String approval_status;
	
	
	public Restaurant getRestaurant() {
		return restaurant;
	}
	public void setRestaurant(Restaurant restaurant) { 
		this.restaurant = restaurant;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getRestaurantName() {
		return restaurantName;
	}
	public void setRestaurantName(String restaurantName) {
		this.restaurantName = restaurantName;
	}
	public Long getSubmission_id() {
		return submission_id;
	}
	public void setSubmission_id(Long submission_id) {
		this.submission_id = submission_id;
	}
	public Long getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}
	public Long getNeighborhood_id() {
		return neighborhood_id;
	}
	public void setNeighborhood_id(Long neighborhood_id) {
		this.neighborhood_id = neighborhood_id;
	}
	public String getRestaurant_phone_number() {
		return restaurant_phone_number;
	}
	public void setRestaurant_phone_number(String restaurant_phone_number) {
		this.restaurant_phone_number = restaurant_phone_number;
	}
	public Date getApproval_date() {
		return approval_date;
	}
	public void setApproval_date(Date date) {
		this.approval_date = date;
	}
	public String getApproval_status() {
		return approval_status;
	}
	public void setApproval_status(String approval_status) {
		this.approval_status = approval_status;
	}
	
	
}
