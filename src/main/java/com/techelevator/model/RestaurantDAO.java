package com.techelevator.model;

import java.util.List;

public interface RestaurantDAO {

	public void saveRestaurant(Restaurant restaurant);
	public void addSpecial(); //arguments?
	public List<Restaurant> getRestaurants();
	public Restaurant getRestaurant(Long restaurantId);
	public Restaurant searchRestaurantByName(String restaurantName);
//	public List<Restaurant> getRestaurantsOrderByRating();

	public List<Restaurant> getRestaurantsOrderByRating();
	public List<Restaurant> getRestaurantsOrderByPrice();
	public List<Restaurant> getRestaurantByNeighborhood(String neighborhood);
}
