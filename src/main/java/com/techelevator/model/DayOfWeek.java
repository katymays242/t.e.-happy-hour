package com.techelevator.model;

import java.time.LocalTime;

public class DayOfWeek {

	public enum Day {
		MONDAY,
		TUESDAY,
		WEDNESDAY,
		THURSDAY,
		FRIDAY,
		SATURDAY,
		SUNDAY
	};
	
private Day day;
private LocalTime openingTime;
private LocalTime closingTime;
private LocalTime startTime;
private LocalTime endTime;

public DayOfWeek() {
	
}

public DayOfWeek(Day day, LocalTime openingTime, LocalTime closingTime) {
	this.day = day;
	this.openingTime = openingTime;
	this.closingTime = closingTime;
}

public DayOfWeek setHappyHours(LocalTime startTime, LocalTime endTime) {
	this.startTime = startTime;
	this.endTime = endTime;
	return this;
}

public Day getDay() {
	return day;
}
public void setDay(Day day) {
	this.day = day;
}
public LocalTime getOpeningTime() {
	return openingTime;
}
public void setOpeningTime(LocalTime openingTime) {
	this.openingTime = openingTime;
}
public LocalTime getClosingTime() {
	return closingTime;
}
public void setClosingTime(LocalTime closingTime) {
	this.closingTime = closingTime;
}

public LocalTime getStartTime() {
	return startTime;
}

public void setStartTime(LocalTime startTime) {
	this.startTime = startTime;
}

public LocalTime getEndTime() {
	return endTime;
}

public void setEndTime(LocalTime endTime) {
	this.endTime = endTime;
}

}
