package com.techelevator.model;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.validator.constraints.NotBlank;

public class Restaurant {
	
	List<DayOfWeek> hours = new ArrayList<DayOfWeek>();
	
	public DayOfWeek getDayOfWeek(DayOfWeek.Day day) {
		for(DayOfWeek dayOfWeek : hours) {
			if(dayOfWeek.getDay().equals(day)) {
				return dayOfWeek;
			}
		}
		return null;
	}
	public void setDayOfWeek(DayOfWeek dayOfWeek) {
		this.hours.add(dayOfWeek);
	}
	
	public List<DayOfWeek> getDayOfWeek() {
		return this.hours;
	}
	
	@NotBlank(message="Restaurant name is required")
	private String restaurantName;
	
	@NotBlank(message="Street address is required")
	private String restaurantAddress;
	
	private String restaurantAddress2;
	
	@NotBlank(message="City is required")
	private String restaurantCity;
	
	@NotBlank(message="State is required")
	private String restaurantState;
	
	@NotBlank(message="Zip code is required")
	private String restaurantZipCode;
	
	private String neighborhood;
	
	private int priceRange;
	
	private String websiteLink;
	
	private String restaurantImage;
	
	private Integer happyHourRating;
	
	public Integer getHappyHourRating() {
		return happyHourRating;
	}
	public void setHappyHourRating(Integer happyHourRating) {
		this.happyHourRating = happyHourRating;
	}
	public int getRestaurantRating() {
		return restaurantRating;
	}
	public void setRestaurantRating(int restaurantRating) {
		this.restaurantRating = restaurantRating;
	}

	private int restaurantRating;
	
	public String getRestaurantImage() {
		return restaurantImage;
	}
	public void setRestaurantImage(String restaurantImage) {
		this.restaurantImage = restaurantImage;
	}

	private int typeOfFood;
	
	private String typeOfFoodString;
	
	public String getTypeOfFoodString() {
		return typeOfFoodString;
	}
	public void setTypeOfFoodString(String typeOfFoodString) {
		this.typeOfFoodString = typeOfFoodString;
	}

	private int neighborhoodId;
	
	@NotBlank(message="Restaurant phone number is required")
	private String phoneNumber;
		
	private boolean ownerVerified;
	
	private Long restaurantId;
	
	private String restaurantText;
	
	private String happyHourText;
	
	private String specials;
	
	public String getNeighborhood() {
		return neighborhood;
	}
	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}
	public int getPriceRange() {
		return priceRange;
	}
	public void setRestaurantZipCode(String restaurantZipCode) {
		this.restaurantZipCode = restaurantZipCode;
	}
	
	public void setPriceRange(int priceRange) {
		this.priceRange = priceRange;
	}
	public String getWebsiteLink() {
		return websiteLink;
	}
	public void setWebsiteLink(String websiteLink) {
		this.websiteLink = websiteLink;
	}
	public boolean isOwnerVerified() {
		return ownerVerified;
	}
	public void setOwnerVerified(boolean ownerVerified) {
		this.ownerVerified = ownerVerified;
	}
	public String getRestaurantAddress2() {
		return restaurantAddress2;
	}
	public void setRestaurantAddress2(String restaurantAddress2) {
		this.restaurantAddress2 = restaurantAddress2;
	}
	
	public Long getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(Long restaurantId) {
		this.restaurantId = restaurantId;
	}
	public String getRestaurantCity() {
		return restaurantCity;
	}
	public void setRestaurantCity(String restaurantCity) {
		this.restaurantCity = restaurantCity;
	}
	public String getRestaurantState() {
		return restaurantState;
	}
	public void setRestaurantState(String restaurantState) {
		this.restaurantState = restaurantState;
	}
	public String getRestaurantZipCode() {
		return restaurantZipCode;
	}
	public String getRestaurantName() {
		return restaurantName;
	}
	public void setRestaurantName(String restaurantName) {
		this.restaurantName = restaurantName;
	}
	public String getRestaurantAddress() {
		return restaurantAddress;
	}
	public void setRestaurantAddress(String restaurantAddress) {
		this.restaurantAddress = restaurantAddress;
	}
	public int getTypeOfFood() {
		return typeOfFood;
	}
	public void setTypeOfFood(int typeOfFood) {
		this.typeOfFood = typeOfFood;
	}
	public int getNeighborhoodId() {
		return neighborhoodId;
	}
	public void setNeighborhoodId(int neighborhoodId) {
		this.neighborhoodId = neighborhoodId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getRestaurantText() {
		return restaurantText;
	}
	public void setRestaurantText(String restaurantText) {
		this.restaurantText = restaurantText;
	}
	public String getHappyHourText() {
		return happyHourText;
	}
	public void setHappyHourText(String happyHourText) {
		this.happyHourText = happyHourText;
	}
	public String getSpecials() {
		return specials;
	}
	public void setSpecials(String specials) {
		this.specials = specials;
	}
	
	
}
