package com.techelevator.model;

import javax.validation.constraints.AssertTrue;

import org.hibernate.validator.constraints.NotBlank;

public class User {


@NotBlank(message="First name is required")
private String firstName;

@NotBlank(message="Last name is required")
private String lastName;

@NotBlank(message="Email address is required")
private String email;

@NotBlank(message="Password is required")
private String password;

@NotBlank(message="Please confirm your password")
private String confirmPassword;

@AssertTrue(message="Passwords must match")
public boolean isPasswordMatching() {
	if(password != null) {
		return password.equals(confirmPassword);
	} else {
		return false;
	}
}

private Long id;
private int accountId;
private String restaurantName;
private int neighborhood;
private String businessPhone;

public Long getId() {
		return id;
	}
public void setId(Long id) {
		this.id = id;
	}
public String getRestaurantName() {
	return restaurantName;
}
public void setRestaurantName(String restaurantName) {
	this.restaurantName = restaurantName;
}
public int getNeighborhood() {
	return neighborhood;
}
public void setNeighborhood(int neighborhood) {
	this.neighborhood = neighborhood;
}
public String getBusinessPhone() {
	return businessPhone;
}
public void setBusinessPhone(String businessPhone) {
	this.businessPhone = businessPhone;
}
public String getFirstName() {
	return firstName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public String getLastName() {
	return lastName;
}
public void setLastName(String lastName) {
	this.lastName = lastName;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getConfirmPassword() {
	return confirmPassword;
}
public void setConfirmPassword(String confirmPassword) {
	this.confirmPassword = confirmPassword;
}
public int getAccountId() {
	return accountId;
}
public void setAccountId(int accountId) {
	this.accountId = accountId;
}


}
