package com.techelevator.model;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.InvalidResultSetAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;


@Component
public class JDBCRestaurantDAO implements RestaurantDAO{
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JDBCRestaurantDAO (DataSource dataSource) {
		this.jdbcTemplate= new JdbcTemplate (dataSource);
	}

	@Override
	public void saveRestaurant(Restaurant restaurant) {
		long id = getNextId();
	
		String sqlInsertRestaurant = "INSERT INTO restaurant(restaurant_id, restaurant_name, addressLine1, addressLine2, city, state, postalCode, restaurant_phone_number, websitelink, restaurant_text, happyhour_text, specials) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
		jdbcTemplate.update(sqlInsertRestaurant, id, restaurant.getRestaurantName(), restaurant.getRestaurantAddress(), restaurant.getRestaurantAddress2(), restaurant.getRestaurantCity(), restaurant.getRestaurantState(), restaurant.getRestaurantZipCode(), restaurant.getPhoneNumber(), restaurant.getWebsiteLink(), restaurant.getRestaurantText(), restaurant.getHappyHourText(), restaurant.getSpecials());
		restaurant.setRestaurantId(id);
		
		String sqlInsertNeighborhood = "INSERT INTO restaurant_neighborhood (restaurant_id, neighborhood_id) VALUES (?,?)";
		jdbcTemplate.update(sqlInsertNeighborhood, restaurant.getRestaurantId(), restaurant.getNeighborhoodId());
		
		String sqlInsertFoodType = "INSERT INTO restaurant_food_type (restaurant_id, food_type_id) VALUES (?,?)";
		jdbcTemplate.update(sqlInsertFoodType, restaurant.getRestaurantId(), restaurant.getTypeOfFood());
		
		String sqlInsertPrice = "INSERT INTO restaurant_price (restaurant_id, price_id) VALUES(?,?)";
		jdbcTemplate.update(sqlInsertPrice, restaurant.getRestaurantId(), restaurant.getPriceRange());
		
		
		for (DayOfWeek dayOfWeek : restaurant.getDayOfWeek()) {
			
			String sqlInsertRestHours = "INSERT INTO restaurant_hours(restaurant_id, day_of_week_id, opening_time,closing_time) VALUES(?,?,?,?)";
			jdbcTemplate.update(sqlInsertRestHours, restaurant.getRestaurantId(), dayOfWeek.getDay().ordinal(), 
					dayOfWeek.getOpeningTime().format(DateTimeFormatter.ofPattern("h:mm a")), 
					dayOfWeek.getClosingTime().format(DateTimeFormatter.ofPattern("h:mm a")));
			
			String sqlInserthappyHours = "INSERT INTO happy_hour_hours(restaurant_id, day_of_week_id, start_time,end_time) VALUES(?,?,?,?)";
			jdbcTemplate.update(sqlInserthappyHours, restaurant.getRestaurantId(), dayOfWeek.getDay().ordinal(),
					dayOfWeek.getStartTime().format(DateTimeFormatter.ofPattern("h:mm a")), 
					dayOfWeek.getEndTime().format(DateTimeFormatter.ofPattern("h:mm a")));	
	
		}
		
	}

	@Override
	public void addSpecial() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Restaurant> getRestaurants() {
		String sqlSelectRestaurants = "SELECT * FROM restaurant "
										+"JOIN restaurant_neighborhood ON restaurant.restaurant_id = restaurant_neighborhood.restaurant_id "
										+ "JOIN neighborhood ON restaurant_neighborhood.neighborhood_id = neighborhood.neighborhood_id "
										+ "JOIN restaurant_food_type ON restaurant.restaurant_id = restaurant_food_type.restaurant_id "
										+ "JOIN food_type ON restaurant_food_type.food_type_id = food_type.food_type_id "
										+ "JOIN restaurant_price ON restaurant.restaurant_id = restaurant_price.restaurant_id "
										+ "JOIN price ON restaurant_price.price_id = price.price_id ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectRestaurants);
		return mapRowSetRestaurants(results);
	}
	
	@Override
	public List<Restaurant> getRestaurantsOrderByRating() {
		String sqlSelectRestaurantsByRating = "SELECT AVG(happy_hour_rating) as rating, restaurant.restaurant_id, restaurant.restaurant_name, addressLine1, "
							+ "food_type.food_type_name, city, state, postalCode, restaurant_phone_number, websiteLink, restaurantimage, restaurant_text, happyhour_text, specials, "
							+ "restaurantimage, neighborhood.neighborhood_id, neighborhood.name, restaurant_food_type.food_type_id, food_type_name, price.price_id, price.price_range "
							+ "FROM restaurant "
							+ "JOIN review ON restaurant.restaurant_id = review.restaurant_id "
							+ "JOIN restaurant_food_type ON restaurant.restaurant_id = restaurant_food_type.restaurant_id "
							+ "JOIN food_type ON restaurant_food_type.food_type_id = food_type.food_type_id "
							+ "JOIN restaurant_neighborhood ON restaurant.restaurant_id = restaurant_neighborhood.restaurant_id "
							+ "JOIN neighborhood ON restaurant_neighborhood.neighborhood_id = neighborhood.neighborhood_id "
							+ "JOIN restaurant_price ON restaurant.restaurant_id = restaurant_price.restaurant_id "
							+ "JOIN price ON restaurant_price.price_id = price.price_id "
							+ "GROUP BY restaurant.restaurant_id, restaurant_food_type.food_type_id, "
							+ "food_type.food_type_name, restaurant.restaurantimage, neighborhood.neighborhood_id, price.price_id "
							+ "ORDER BY rating DESC";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectRestaurantsByRating);
		return mapRowSetRestaurants(results);
	}
	
	@Override
	public List<Restaurant> getRestaurantsOrderByPrice() {
		String sqlSelectRestaurantsByPrice = "SELECT * FROM restaurant "
				+ "JOIN restaurant_neighborhood ON restaurant.restaurant_id = restaurant_neighborhood.restaurant_id "
				+ "JOIN neighborhood ON restaurant_neighborhood.neighborhood_id = neighborhood.neighborhood_id "
				+ "JOIN restaurant_food_type ON restaurant.restaurant_id = restaurant_food_type.restaurant_id "
				+ "JOIN food_type ON restaurant_food_type.food_type_id = food_type.food_type_id "
				+ "JOIN restaurant_price ON restaurant.restaurant_id = restaurant_price.restaurant_id "
				+ "JOIN price ON restaurant_price.price_id = price.price_id "
				+ "ORDER BY price.price_id ASC";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectRestaurantsByPrice);
		return mapRowSetRestaurants(results);
	}
	
	@Override
	public Restaurant getRestaurant(Long restaurantId) {
		String sqlSelectRestaurants = "SELECT * FROM restaurant "
										+"JOIN restaurant_neighborhood ON restaurant.restaurant_id = restaurant_neighborhood.restaurant_id "
										+ "JOIN neighborhood ON restaurant_neighborhood.neighborhood_id = neighborhood.neighborhood_id "
										+ "JOIN restaurant_food_type ON restaurant.restaurant_id = restaurant_food_type.restaurant_id "
										+ "JOIN food_type ON restaurant_food_type.food_type_id = food_type.food_type_id "
										+ "JOIN restaurant_price ON restaurant.restaurant_id = restaurant_price.restaurant_id "
										+ "JOIN price ON restaurant_price.price_id = price.price_id "
										+ "WHERE restaurant.restaurant_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectRestaurants, restaurantId);
		if(results.next()) {
			return mapRowToRestaurant(results);
		} else {
			return null;
		}
	}

	
	
	
	@Override
	public Restaurant searchRestaurantByName(String restaurantName) {
			Restaurant restaurant = null;
			String sqlSelectRestaurantsByName = "SELECT * FROM restaurant WHERE LOWER(restaurant_name) = ?";
			SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectRestaurantsByName, restaurantName);
			if(results.next()) {
				restaurant = mapRowToRestaurantInfo(results);
				
			} 
			return restaurant;
		}
	
	private Restaurant mapRowToRestaurantInfo(SqlRowSet results) {
		Restaurant restaurant = new Restaurant();
		restaurant.setRestaurantName(results.getString("restaurant_name"));
		restaurant.setRestaurantAddress(results.getString("addressline1"));
		restaurant.setRestaurantCity(results.getString("city"));
		restaurant.setRestaurantState(results.getString("state"));
		restaurant.setRestaurantZipCode(results.getString("postalcode"));
		restaurant.setPhoneNumber(results.getString("restaurant_phone_number"));
		restaurant.setRestaurantId(results.getLong("restaurant_id"));
		
		return restaurant;
	}
	
	private List<Restaurant> mapRowSetRestaurants(SqlRowSet results) {
		ArrayList<Restaurant> restaurantList = new ArrayList<>();
		while (results.next()) {
			restaurantList.add(mapRowToRestaurant(results));
		}
		return restaurantList;
	}
	
	private List<Restaurant> mapRowSetRestaurants1(SqlRowSet results) {
		ArrayList<Restaurant> restaurantList = new ArrayList<>();
		restaurantList.add(mapRowToRestaurantNeighborhood(results));
		while (results.next())  {
			restaurantList.add(mapRowToRestaurantNeighborhood(results));
		}
		return restaurantList;
	}
	


		
	private Restaurant mapRowToRestaurant(SqlRowSet results) {
		Restaurant restaurant = new Restaurant();
		restaurant.setRestaurantName(results.getString("restaurant_name"));
		restaurant.setRestaurantAddress(results.getString("addressline1"));
		restaurant.setRestaurantCity(results.getString("city"));
		restaurant.setRestaurantState(results.getString("state"));
		restaurant.setRestaurantZipCode(results.getString("postalcode"));
		restaurant.setPhoneNumber(results.getString("restaurant_phone_number"));
		restaurant.setRestaurantId(results.getLong("restaurant_id"));
		restaurant.setNeighborhoodId(results.getInt("neighborhood_id"));
		restaurant.setNeighborhood(results.getString("name"));
		restaurant.setRestaurantImage(results.getString("restaurantimage"));
		restaurant.setTypeOfFood(results.getInt("food_type_id"));
		restaurant.setTypeOfFoodString(results.getString("food_type_name"));
		restaurant.setPriceRange(results.getInt("price_id"));
		restaurant.setWebsiteLink(results.getString("websitelink"));
		restaurant.setRestaurantText(results.getString("restaurant_text"));
		restaurant.setHappyHourText(results.getString("happyhour_text"));
		restaurant.setSpecials(results.getString("specials"));
		
		try{
			restaurant.setHappyHourRating(results.getInt("rating"));
		} catch(InvalidResultSetAccessException e) {
			// This query doesn't have average rating, no biggie.
		}
		
		return restaurant;
	}
	
	private Restaurant mapRowToRestaurantNeighborhood(SqlRowSet results) {
		Restaurant restaurant = new Restaurant();
		restaurant.setRestaurantName(results.getString("restaurant_name"));
		restaurant.setRestaurantAddress(results.getString("addressline1"));
		restaurant.setRestaurantCity(results.getString("city"));
		restaurant.setRestaurantState(results.getString("state"));
		restaurant.setRestaurantZipCode(results.getString("postalcode"));
		restaurant.setPhoneNumber(results.getString("restaurant_phone_number"));
		restaurant.setRestaurantId(results.getLong("restaurant_id"));
		restaurant.setNeighborhoodId(results.getInt("neighborhood_id"));
		restaurant.setNeighborhood(results.getString("name"));
		restaurant.setRestaurantImage(results.getString("restaurantimage"));
		restaurant.setPriceRange(results.getInt("price_id"));
		restaurant.setWebsiteLink(results.getString("websitelink"));
		restaurant.setRestaurantText(results.getString("restaurant_text"));
		restaurant.setHappyHourText(results.getString("happyhour_text"));
		restaurant.setSpecials(results.getString("specials"));
		
		
		return restaurant;
	}
	
	

	private Long getNextId() {
		String sqlSelectNextId = "SELECT NEXTVAL('restaurant_restaurant_id_seq')";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectNextId);
		Long id = null;
		if(results.next()) {
			id = results.getLong(1);
		} else {
			throw new RuntimeException("Something strange happened, unable to select next forum post id from sequence");
		}
		return id;
	}

	private int getNextReviewId() {
		
		String sqlSelectNextReviewId = "SELECT NEXTVAL('review_review_id_seq')";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectNextReviewId);
		Integer reviewId=null;
		if(results.next()) {
			reviewId = results.getInt(1);
		} else {
			throw new RuntimeException("Something strange happened, unable to select next forum post id from sequence");
		}
		return reviewId;
	}

	@Override
	public List<Restaurant> getRestaurantByNeighborhood(String neighborhood) {
		String sqlSelectRestaurants = "SELECT restaurant.restaurant_id, restaurant.restaurant_name, "
				+ "restaurantimage, addressLine1, city, state, postalCode, restaurant_phone_number, ownerVerified, websiteLink, restaurantimage, restaurant_text, happyhour_text, neighborhood.neighborhood_id, "
				+ "neighborhood.name, price.price_id, price.price_range, specials "
				+ "FROM restaurant "
				+ "JOIN restaurant_neighborhood ON restaurant.restaurant_id = restaurant_neighborhood.restaurant_id "
				+ "JOIN neighborhood ON restaurant_neighborhood.neighborhood_id = neighborhood.neighborhood_id "
				+ "JOIN restaurant_price ON restaurant.restaurant_id = restaurant_price.restaurant_id "
				+ "JOIN price ON restaurant_price.price_id = price.price_id "
				+ "WHERE neighborhood.name = ?";
		
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectRestaurants, neighborhood);
			if(results.next()) {
					return mapRowSetRestaurants1(results);
			} else {
				return null;
			}
	}
}

	
		

