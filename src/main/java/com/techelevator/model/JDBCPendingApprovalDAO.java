package com.techelevator.model;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

@Component
public class JDBCPendingApprovalDAO implements PendingApprovalDAO{

	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	public JDBCPendingApprovalDAO(DataSource dataSource) {
		this.jdbcTemplate= new JdbcTemplate (dataSource);
	}
	@Override
	public void updatePendingOwnerStatus(PendingApproval decision) {
		String sqlUpdateApprovalStatus = "UPDATE pending_owners SET approval_status = ? WHERE submission_id = ? ";
		jdbcTemplate.update(sqlUpdateApprovalStatus, decision.getApproval_status(), decision.getSubmission_id());
	}
	
	@Override
	public List<PendingApproval> getPendingApproval() {
			String sqlSelectPendingApproval = "SELECT * FROM pending_owners " 
											+ "JOIN customer ON pending_owners.customer_id = customer.customer_id "
				                        	+ "WHERE pending_owners.approval_status = 'pending'";
			SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectPendingApproval);
			return mapRowSetApprovals(results);
		}
	
	private List<PendingApproval> mapRowSetApprovals(SqlRowSet results) {
		ArrayList<PendingApproval> approvalList = new ArrayList<>();
		while (results.next()) {
			PendingApproval approval = mapRowtoApprovals(results);
			approval.setUser(mapRowtoUser(results));
			approvalList.add(approval);
		}
		return approvalList;
	}

	private User mapRowtoUser(SqlRowSet results) {
		User user = new User();
		user.setId(results.getLong("customer_id"));
		user.setAccountId(results.getInt("account_id"));
		user.setFirstName(results.getString("first_name"));
		user.setLastName(results.getString("last_name"));
		user.setRestaurantName(results.getString("restaurant_name"));
		return user;
	}

//	private Restaurant mapRowtoRestaurant(SqlRowSet results) {
//		Restaurant restaurant = new Restaurant();
//		restaurant.setRestaurantId(results.getLong("restaurant_id"));
//		return restaurant;
//	}
	
	private PendingApproval mapRowtoApprovals(SqlRowSet results) {
		PendingApproval approval = new PendingApproval();

		approval.setApproval_date(results.getDate("approval_date"));
		approval.setApproval_status(results.getString("approval_status"));
		approval.setCustomer_id(results.getLong("customer_id"));
		approval.setNeighborhood_id(results.getLong("neighborhood_id"));
		approval.setRestaurant_phone_number(results.getString("restaurant_phone_number"));
		approval.setSubmission_id(results.getLong("submission_id"));
		approval.setRestaurantName(results.getString("restaurant_name"));
		return approval;
		
	}
	@Override
	public void moveFromPendingToRestaurantOwner(User user){
		String sqlUpdateOwnerStatus = "UPDATE customer SET account_id = 2 WHERE customer_id = ? ";
		jdbcTemplate.update(sqlUpdateOwnerStatus, user.getId());
		
//		String sqlDeleteOwnerFromPending = "DELETE FROM pending_owners WHERE customer_id = ?";
//		jdbcTemplate.update(sqlDeleteOwnerFromPending, user.getId());
//		
//		String sqlUpdateRestaurantOwner = "UPDATE restaurant SET ownerverified = true WHERE restaurant_id = ? ";
//		jdbcTemplate.update(sqlUpdateRestaurantOwner, restaurant.getRestaurantId());
//		
//		String sqlUpdateOwnerShipTable = "INSERT INTO ownership (restaurant_id, customer_id) VALUES (?,?);";
//		jdbcTemplate.update(sqlUpdateOwnerShipTable,restaurant.getRestaurantId(), user.getId());

	}


}


