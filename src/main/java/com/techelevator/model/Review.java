package com.techelevator.model;

public class Review {

	private int reviewID;
	private int restaurantRating;
	private int happyHourRating;
	private String reviewText;
	private boolean flagged;
	private long restaurantId;
	private long customerID;
	
	
	public Long getRestaurantId() {
		return restaurantId;
	}
	public void setRestaurantId(Long restaurantId) {
		this.restaurantId = restaurantId;
	}
	public int getReviewID() {
		return reviewID;
	}
	public void setReviewID(int reviewID) {
		this.reviewID = reviewID;
	}
	public int getRestaurantRating() {
		return restaurantRating;
	}
	public void setRestaurantRating(int restaurantRating) {
		this.restaurantRating = restaurantRating;
	}
	public int getHappyHourRating() {
		return happyHourRating;
	}
	public void setHappyHourRating(int happyHourRating) {
		this.happyHourRating = happyHourRating;
	}
	public String getReviewText() {
		return reviewText;
	}
	public void setReviewText(String reviewText) {
		this.reviewText = reviewText;
	}
	public boolean isFlagged() {
		return flagged;
	}
	public void setFlagged(boolean flagged) {
		this.flagged = flagged;
	}
	public long getCustomerID() {
		return customerID;
	}
	public void setCustomerID(long customerID) {
		this.customerID = customerID;
	}
	

}
