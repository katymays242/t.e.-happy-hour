package com.techelevator.model;

import java.util.List;

public interface PendingApprovalDAO {

	public List<PendingApproval> getPendingApproval();
	public void updatePendingOwnerStatus(PendingApproval decision);
	public void moveFromPendingToRestaurantOwner(User user);
}
