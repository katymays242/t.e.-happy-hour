package com.techelevator.model;
import java.util.ArrayList;
import java.util.List;


import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;



@Component
public class JDBCReviewDAO implements ReviewDAO{
	
	private JdbcTemplate jdbcTemplate;
	
	
@Autowired
public JDBCReviewDAO (DataSource dataSource) {
		this.jdbcTemplate= new JdbcTemplate (dataSource);
	}



	@Override
	
	
	public void saveReview(Review review ) {
	
		String sqlInsertNewReview = "INSERT INTO review(restaurant_rating, happy_hour_rating, review_text, customer_id, restaurant_id)"
				+ "VALUES(?,?,?,?,?)";
		jdbcTemplate.update(sqlInsertNewReview, review.getRestaurantRating(), review.getHappyHourRating(), review.getReviewText(),review.getCustomerID(),
				review.getRestaurantId());
		
	}

	@Override
	public List<Review> getReview(Restaurant restaurant) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Review> getAllReviews() {
		String sqlReviewQuery = "SELECT * FROM review WHERE ";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlReviewQuery);
		return mapRowSetToReviews(results);
		
	}
	
	@Override
	public List<Review> getReviewsByRestaurant(Review review) {
		String sqlSelectReviewsByRestaurant = "SELECT * FROM review WHERE restaurant_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectReviewsByRestaurant, review.getRestaurantId());
			return mapRowSetToReviews(results);
	}
	
	private List<Review> mapRowSetToReviews(SqlRowSet results) {
		ArrayList<Review> reviewList = new ArrayList<>();
		while (results.next()) {
			Review review = mapRowToReview(results);
			reviewList.add(review);
		}
		return reviewList;
	}

		
private Review mapRowToReview(SqlRowSet results) {
		Review review = new Review();
		review.setReviewID(results.getInt("review_id"));
		review.setHappyHourRating(results.getInt("happy_hour_rating"));
		review.setRestaurantRating(results.getInt("restaurant_rating"));
		review.setReviewText(results.getString("review_text"));
		review.setCustomerID(results.getLong("customer_id"));
		return review;
	}



@Override
public long getCustomerIDByEmail(Object email) {
	String sqlSelectCustomerID = "SELECT customer_id FROM customer WHERE email = ?";
	SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSelectCustomerID, email);
	List<Long> rs = new ArrayList<Long>();
	
	while(results.next()){
		rs.add(results.getLong("customer_id"));
	}
	long id = rs.get(0);
	
	return id;
	
}

}






