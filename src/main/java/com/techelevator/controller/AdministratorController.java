package com.techelevator.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.model.PendingApproval;
import com.techelevator.model.PendingApprovalDAO;
import com.techelevator.model.User;

@Controller
@SessionAttributes("email")
public class AdministratorController {


		private PendingApprovalDAO pendingApprovalDAO;

		@Autowired
		public AdministratorController(PendingApprovalDAO pendingApprovalDAO) {
			
			this.pendingApprovalDAO = pendingApprovalDAO;
		}
		
		@RequestMapping(path="/administrator", method=RequestMethod.GET)
		public String displayRestaurantDetailPage(HttpServletRequest request){
			
			List<PendingApproval> pendingList = pendingApprovalDAO.getPendingApproval();
			request.setAttribute("pendingList", pendingList);
			
			
			return "administrator";
		}
		
		@RequestMapping(path = "/pendingOwner", method = RequestMethod.POST)
		public String setOrChangeOwnerStatus(@RequestParam String approval,
											@RequestParam long id,
											@RequestParam Long userId,
											ModelMap map
											){
			
			PendingApproval decision = new PendingApproval();
			decision.setApproval_status(approval);
			decision.setSubmission_id(id);
			pendingApprovalDAO.updatePendingOwnerStatus(decision);
			
			User user = new User();
			user.setId(userId);
			
			pendingApprovalDAO.moveFromPendingToRestaurantOwner(user);
			
			return "redirect:/administrator";
			
			
		}

	}

