package com.techelevator.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.techelevator.model.Restaurant;
import com.techelevator.model.RestaurantDAO;

@Controller
@SessionAttributes("email")
public class HomeController {
	private RestaurantDAO restaurantDAO;

	@Autowired
	public HomeController(RestaurantDAO restaurantDAO) {
		this.restaurantDAO = restaurantDAO;
	}

	@RequestMapping(path = "/")
	public String displayHomepage(HttpServletRequest request) {
		List<Restaurant> restaurants = restaurantDAO.getRestaurants();
		request.setAttribute("restaurantList", restaurants);
		return "homepage";
	}
	
	@RequestMapping(path = "/ratings", method=RequestMethod.GET)
	public String displayRatingsHomepage(HttpServletRequest request) {
		List<Restaurant> restaurants = restaurantDAO.getRestaurantsOrderByRating();
		request.setAttribute("restaurantListRating", restaurants);
		return "ratings";
	}
	
	@RequestMapping(path = "/prices", method=RequestMethod.GET)
	public String displayPricesHomepage(HttpServletRequest request) {
		List<Restaurant> restaurants = restaurantDAO.getRestaurantsOrderByPrice();
		request.setAttribute("restaurantListPrice", restaurants);
		return "prices";
	}
	
	@RequestMapping(path = "/neighborhood", method=RequestMethod.GET)
	public String displayNeighborhoodHomepage(HttpServletRequest request, @RequestParam String neighborhood) {
		List<Restaurant> restaurants = restaurantDAO.getRestaurantByNeighborhood(neighborhood);
		request.setAttribute("restaurantListNeighborhood", restaurants);
		return "neighborhood";
	}
}
