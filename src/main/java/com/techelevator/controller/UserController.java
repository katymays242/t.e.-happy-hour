package com.techelevator.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.techelevator.model.User;
import com.techelevator.model.UserDAO;

@Controller
@SessionAttributes("email")
public class UserController {

	private UserDAO userDAO;

	@Autowired
	public UserController(UserDAO userDAO) {
		this.userDAO = userDAO;
	}


	@RequestMapping(path = "/register", method = RequestMethod.GET)
	public String showRegisterForm(Model modelHolder) {
		if (!modelHolder.containsAttribute("user")) {
			modelHolder.addAttribute("user", new User());
		}

		return "register";
	}

	@RequestMapping(path = "/submit", method = RequestMethod.POST)
	public String handleRegistrationForm(@Valid @ModelAttribute("user") User user,
										BindingResult result,
										RedirectAttributes attr) {
		attr.addFlashAttribute("user", user);
		if (result.hasErrors()) {
			attr.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "user", result);
			return "redirect:/register";
		}

		userDAO.saveUser(user);

		if(user.getRestaurantName().isEmpty()) {
			return "redirect:/login";
			
		} else {
			userDAO.pendingOwner(user);
			return "redirect:/beingVerified";
		}
	}
	
	
	@RequestMapping(path="/beingVerified", method=RequestMethod.GET)
	public String displayBeingVerifiedLandingPage() {
		return "beingVerified";
	}
	

	

}
