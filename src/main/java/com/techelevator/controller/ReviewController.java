package com.techelevator.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.techelevator.model.Restaurant;
import com.techelevator.model.Review;
import com.techelevator.model.ReviewDAO;
import com.techelevator.model.UserDAO;

@Controller
@SessionAttributes("email")
public class ReviewController {
	private ReviewDAO reviewDAO;
	
	

	@Autowired
	public ReviewController(ReviewDAO reviewDAO, UserDAO userDAO) {
		this.reviewDAO = reviewDAO;

	}

	@RequestMapping(path = "/reviewResult", method = RequestMethod.GET)
	public String showRestaurantReviews(HttpServletRequest request) {
		List<Review> reviewList = reviewDAO.getAllReviews();
		request.setAttribute("reviewList", reviewList);

		return "reviewResult";
	}

	@RequestMapping(path = "/submitReview", method = RequestMethod.POST)
	public String handleReviewForm(@Valid @ModelAttribute("review") Review review, Restaurant restaurant,
			 ModelMap map, BindingResult result, RedirectAttributes attr,
			@RequestParam int star,
			@RequestParam int martini,
			@RequestParam String reviewText,
			@RequestParam long restaurantId) {

		attr.addFlashAttribute("review", review);
		if (result.hasErrors()) {
			attr.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "review", result);
			return "redirect:/restaurantDetail";
		}
		
		if(map.get("email") != null){
		
		long customerID = reviewDAO.getCustomerIDByEmail(map.get("email"));
		
		review.setCustomerID(customerID);
		review.setHappyHourRating(martini);
		review.setRestaurantId(restaurantId);
		review.setRestaurantRating(star);
		review.setReviewText(reviewText);
		
	
		reviewDAO.saveReview(review);
		return "redirect:/restaurantDetail?restaurantId=" + restaurantId;
	}else{
		return "redirect:/login";
	}
	}
}
