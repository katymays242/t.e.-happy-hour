package com.techelevator.controller;



import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.techelevator.model.DayOfWeek;
import com.techelevator.model.Restaurant;
import com.techelevator.model.RestaurantDAO;
import com.techelevator.model.Review;
import com.techelevator.model.ReviewDAO;


@Controller
@SessionAttributes("email")
public class RestaurantController {

	private RestaurantDAO restaurantDAO;
	private ReviewDAO reviewDAO;

	@Autowired
	public RestaurantController(RestaurantDAO restaurantDAO, ReviewDAO reviewDAO) {
		this.restaurantDAO = restaurantDAO;
		this.reviewDAO = reviewDAO;
	}
	
	@RequestMapping(path="/restaurantDetail", method=RequestMethod.GET)
	public String displayRestaurantDetailPage(HttpServletRequest request ){
		Long restaurantId = Long.parseLong(request.getParameter("restaurantId"));
		Restaurant restaurant = restaurantDAO.getRestaurant(restaurantId);
	
		Review review = new Review();
		review.setRestaurantId(restaurantId);
		
		List <Review> reviewList = reviewDAO.getReviewsByRestaurant(review);
		
		request.setAttribute("restaurant", restaurant);
		request.setAttribute("reviewList", reviewList);

		return "restaurantDetail";
	}
	

	@RequestMapping(path="/searchResults", method=RequestMethod.POST)
		public String displaySearchResults(HttpServletRequest request,
				ModelMap map){
		
		
		String searchWord = request.getParameter("srchterm").toLowerCase();
		Restaurant restaurantInfo = restaurantDAO.searchRestaurantByName(searchWord);
			long restaurantID = restaurantInfo.getRestaurantId();
				
			return "redirect:/restaurantDetail?restaurantId=" + restaurantID;
			
	}
	
	@RequestMapping(path="/searchResults", method=RequestMethod.GET)
	public String displaySearchResultsPage(){
		return "searchResults";
	}
	
	
	@RequestMapping(path="/submitRestaurant", method=RequestMethod.GET)
	public String displaySubmitRestaurant(Model modelHolder) {
		if (!modelHolder.containsAttribute("newRestaurant")) {
			modelHolder.addAttribute("newRestaurant", new Restaurant());
		}
		return "submitRestaurant";
	}

	
	@RequestMapping(path = "/submitRestaurant", method = RequestMethod.POST)
	public String submitNewRestaurant(@Valid @ModelAttribute("newRestaurant") 
										Restaurant restaurant,
										@RequestParam String MondayOpeningTime,
										@RequestParam String MondayClosingTime,
										@RequestParam String TuesdayOpeningTime,
										@RequestParam String TuesdayClosingTime,
										@RequestParam String WednesdayOpeningTime,
										@RequestParam String WednesdayClosingTime,
										@RequestParam String ThursdayOpeningTime,
										@RequestParam String ThursdayClosingTime,
										@RequestParam String FridayOpeningTime,
										@RequestParam String FridayClosingTime,
										@RequestParam String SaturdayOpeningTime,
										@RequestParam String SaturdayClosingTime,
										@RequestParam String SundayOpeningTime,
										@RequestParam String SundayClosingTime,
										@RequestParam String MondayHappyHourStart,
										@RequestParam String MondayHappyHourEnd,
										@RequestParam String TuesdayHappyHourStart,
										@RequestParam String TuesdayHappyHourEnd,
										@RequestParam String WednesdayHappyHourStart,
										@RequestParam String WednesdayHappyHourEnd,
										@RequestParam String ThursdayHappyHourStart,
										@RequestParam String ThursdayHappyHourEnd,
										@RequestParam String FridayHappyHourStart,
										@RequestParam String FridayHappyHourEnd,
										@RequestParam String SaturdayHappyHourStart,
										@RequestParam String SaturdayHappyHourEnd,
										@RequestParam String SundayHappyHourStart,
										@RequestParam String SundayHappyHourEnd,
										BindingResult result,
										RedirectAttributes attr, 
										ModelMap map) {
		attr.addFlashAttribute("newRestaurant", restaurant);	
		
		if (result.hasErrors()) {
			attr.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "newRestaurant", restaurant);
			return "redirect:/submitRestaurant";
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("h:mm a");
	
		
		restaurant.setDayOfWeek(
				new DayOfWeek( 	DayOfWeek.Day.MONDAY, 
								LocalTime.parse(MondayOpeningTime, formatter), 
								LocalTime.parse(MondayClosingTime, formatter)
					).setHappyHours(
							LocalTime.parse(MondayHappyHourStart, formatter), 
							LocalTime.parse(MondayHappyHourEnd, formatter)
					)
				);

		restaurant.setDayOfWeek(
				new DayOfWeek( 	DayOfWeek.Day.TUESDAY, 
								LocalTime.parse(TuesdayOpeningTime, formatter), 
								LocalTime.parse(TuesdayClosingTime, formatter)
					).setHappyHours(
							LocalTime.parse(TuesdayHappyHourStart, formatter), 
							LocalTime.parse(TuesdayHappyHourEnd, formatter)
					)
				);
		
		restaurant.setDayOfWeek(
				new DayOfWeek( 	DayOfWeek.Day.WEDNESDAY, 
								LocalTime.parse(WednesdayOpeningTime, formatter), 
								LocalTime.parse(WednesdayClosingTime, formatter)
					).setHappyHours(
							LocalTime.parse(WednesdayHappyHourStart, formatter), 
							LocalTime.parse(WednesdayHappyHourEnd, formatter)
					)
				);
		
		restaurant.setDayOfWeek(
				new DayOfWeek( 	DayOfWeek.Day.THURSDAY, 
								LocalTime.parse(ThursdayOpeningTime, formatter), 
								LocalTime.parse(ThursdayClosingTime, formatter)
					).setHappyHours(
							LocalTime.parse(ThursdayHappyHourStart, formatter), 
							LocalTime.parse(ThursdayHappyHourEnd, formatter)
					)
				);
		
		restaurant.setDayOfWeek(
				new DayOfWeek( 	DayOfWeek.Day.FRIDAY, 
								LocalTime.parse(FridayOpeningTime, formatter), 
								LocalTime.parse(FridayClosingTime, formatter)
					).setHappyHours(
							LocalTime.parse(FridayHappyHourStart, formatter), 
							LocalTime.parse(FridayHappyHourEnd, formatter)
					)
				);

		restaurant.setDayOfWeek(
				new DayOfWeek( 	DayOfWeek.Day.SATURDAY, 
								LocalTime.parse(SaturdayOpeningTime, formatter), 
								LocalTime.parse(SaturdayClosingTime, formatter)
					).setHappyHours(
							LocalTime.parse(SaturdayHappyHourStart, formatter), 
							LocalTime.parse(SaturdayHappyHourEnd, formatter)
					)
				);

		
		restaurant.setDayOfWeek(
				new DayOfWeek( 	DayOfWeek.Day.SUNDAY, 
								LocalTime.parse(SundayOpeningTime, formatter), 
								LocalTime.parse(SundayClosingTime, formatter)
					).setHappyHours(
							LocalTime.parse(SundayHappyHourStart, formatter), 
							LocalTime.parse(SundayHappyHourEnd, formatter)
					)
				);
		
		restaurantDAO.saveRestaurant(restaurant);
		
		return "redirect:/";
	}
}